#-------------------------------------------------
#
# Project created by QtCreator 2014-01-10T14:45:44
#
#-------------------------------------------------

QT       += core gui sql printsupport serialport declarative webkit webkitwidgets network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Proba
TEMPLATE = app

SOURCES +=  main.cpp\
            mainwindow.cpp \
            mysql.cpp \
            logindialog.cpp \
            usercontrolandlogging.cpp \
    qcustomplot.cpp \
    adminTHwidget.cpp \
    smtp.cpp \
    mapwidget.cpp \
    logwidget.cpp \
    thwidget.cpp \
    plottingwidget.cpp \
    commandwidget.cpp \
    thtcpserver.cpp \
    lgtcpserver.cpp \
    lgwidget.cpp \
    getsetcombobox.cpp

HEADERS  += mainwindow.h \
            mysql.h \
            logindialog.h \
            usercontrolandlogging.h \
    qcustomplot.h \
    adminTHwidget.h \
    smtp.h \
    mapwidget.h \
    logwidget.h \
    thwidget.h \
    plottingwidget.h \
    commandwidget.h \
    thtcpserver.h \
    lgtcpserver.h \
    lgwidget.h \
    getsetcombobox.h

FORMS    += mainwindow.ui \
            logindialog.ui \
    adminTHwidget.ui \
    mapwidget.ui \
    logwidget.ui \
    thwidget.ui \
    plottingwidget.ui \
    commandwidget.ui \
    lgwidget.ui

RESOURCES += \
    resources.qrc
RC_FILE = myapp.rc

OTHER_FILES +=

SUBDIRS += \
    configWizard.pro
