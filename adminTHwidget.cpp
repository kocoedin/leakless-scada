#include "adminTHwidget.h"
#include "ui_adminTHwidget.h"

adminTHwidget::adminTHwidget(QMainWindow *parent, mySQL *Database) : QTabWidget(parent), ui(new Ui::adminTHwidget)
{
    ui->setupUi(this);

    m_database=Database;
    refreshDatabase();

}

adminTHwidget::~adminTHwidget()
{
    delete ui;
}

void adminTHwidget::refreshDatabase()
{
    QStringList list = m_database->database.tables(QSql::Tables);

    for(int i =0; i<list.size();i++){
         ui->databaseList->addItem(list.at(i));
    }
}

void adminTHwidget::on_databaseList_itemClicked(QListWidgetItem *item)
{
    QString itemName=item->text();
    model=new QSqlTableModel(this);
    model->setTable(itemName);
    model->select();
    ui->tableView->setModel(model);
}

void adminTHwidget::showWidget(int TabNumber){
    QMdiSubWindow *parent= qobject_cast<QMdiSubWindow *>(this->parent());
    setCurrentIndex(TabNumber);
    parent->show();
}
