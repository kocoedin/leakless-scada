#ifndef adminTHwidget_H
#define adminTHwidget_H

#include <QTabWidget>
#include <QListWidgetItem>
#include <QTabWidget>
#include <QtSerialPort/QSerialPortInfo>
#include <QIntValidator>
#include <QLineEdit>
#include "plottingwidget.h"
#include "thwidget.h"
#include "thtcpserver.h"
#include "mapwidget.h"
#include "getsetcombobox.h"


namespace Ui {
class adminTHwidget;
}

class adminTHwidget : public QTabWidget
{
    Q_OBJECT

public:
    explicit adminTHwidget(QMainWindow *parent = 0, mySQL *Database=0);
    ~adminTHwidget();


    void refreshDatabase();
    void showWidget(int TabNumber);

private slots:
    void on_databaseList_itemClicked(QListWidgetItem *item);


private:

    Ui::adminTHwidget *ui;
    QSqlTableModel *model;
    mySQL *m_database;


};

#endif // adminTHwidget_H
