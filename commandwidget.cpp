#include "commandwidget.h"
#include "ui_commandwidget.h"

CommandWidget::CommandWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::CommandWidget)
{
    ui->setupUi(this);
    ui->commadListTree->expandAll();
}

CommandWidget::~CommandWidget()
{
    delete ui;
}

void CommandWidget::on_commadListTree_itemClicked(QTreeWidgetItem *item, int column)
{
    if (item->isDisabled()){
        return;
    }

    /* Ovo je provjera da se uvijek odabere onaj item koji nema djece */
    if (item->childCount()!=0 && item->text(0)!="Overview"){
        item=item->child(0);
        ui->commadListTree->setCurrentItem(item,0,QItemSelectionModel::ClearAndSelect); // Postavi selection na childa
        return;
    }

    emit commandItemClicked(item);
}
