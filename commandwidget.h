#ifndef COMMANDWIDGET_H
#define COMMANDWIDGET_H

#include <QDockWidget>
#include <QTreeWidget>

namespace Ui {
class CommandWidget;
}

class CommandWidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit CommandWidget(QWidget *parent = 0);
    ~CommandWidget();

private slots:
    void on_commadListTree_itemClicked(QTreeWidgetItem *item, int column);

signals:
    void commandItemClicked(QTreeWidgetItem *item);

private:
    Ui::CommandWidget *ui;
};

#endif // COMMANDWIDGET_H
