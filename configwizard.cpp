#include "configwizard.h"
#include "ui_configwizard.h"

configWizard::configWizard(QWidget *parent,THDetectorController *THController) :
    QDialog(parent),
    ui(new Ui::configWizard)
{

    m_THController=THController;
    ui->setupUi(this);
    AddressOk=false;
    NameOk=false;
    FileOk=false;
    RTCCalibOk=true;
    ui->pushButton->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
    RTCCalib=ui->rtcEdit->text().toInt();
    ui->rtcLabel->setText("OK");
    ui->firstFrame->show();
    ui->finalFrame->hide();
    m_SIM900ConfigStarted=false;
    m_SIM900ConfigSuccessfull=false;

    connect(THController,SIGNAL(THResponded(QString)), this, SLOT(THDataRecieved(QString)));
}

configWizard::~configWizard()
{
    delete ui;
}

void configWizard::on_addressEdit_textChanged(const QString &arg1)
{
    bool ok;
    if (arg1.toInt(&ok,16)>255){
        ui->addressLabel->setText("Error, > 0xFF");
        ui->pushButton->setEnabled(false);
        AddressOk=false;
        return;
    }
    if (!ok){
        ui->addressLabel->setText("Error, Not a number");
        ui->pushButton->setEnabled(false);
        AddressOk=false;
        return;
    }

    AddressOk=true;

    if (AddressOk && FileOk && NameOk && RTCCalibOk) ui->pushButton->setEnabled(true);
    Address=ui->addressEdit->text();
    ui->addressLabel->setText("OK");
}

void configWizard::on_nameEdit_textChanged(const QString &arg1)
{
    if (arg1.length()>9){
        ui->nameLabel->setText("Error, Too long");
        ui->pushButton->setEnabled(false);
        NameOk=false;
        return;
    }
    if (arg1.contains(" ",Qt::CaseInsensitive)){

        ui->nameLabel->setText("Error, Must not contain spaces");
        ui->pushButton->setEnabled(false);
        NameOk=false;
        return;
    }

    NameOk=true;
    if (AddressOk && FileOk && NameOk && RTCCalibOk) ui->pushButton->setEnabled(true);
    Name=ui->nameEdit->text();
    ui->nameLabel->setText("OK");
}

void configWizard::on_configEdit_textChanged(const QString &arg1)
{

    if (arg1==""){
        ui->configLabel->setText("Error, Choose File");
        ui->pushButton->setEnabled(false);
        FileOk=false;
        return;
    }

    ui->configLabel->setText("OK");
    FileOk=true;
    FileLoc=arg1;
    if (AddressOk && FileOk && NameOk && RTCCalibOk) ui->pushButton->setEnabled(true);
}

void configWizard::on_rtcEdit_textChanged(const QString &arg1)
{
    bool ok;
    ui->rtcEdit->text().toInt(&ok,10);
    if(!ok){
        ui->rtcLabel->setText("Error, Not a number");
        ui->pushButton->setEnabled(false);
        RTCCalibOk=false;
        return;
    }

    RTCCalib=true;
    if (AddressOk && FileOk && NameOk && RTCCalibOk) ui->pushButton->setEnabled(true);
    ui->rtcLabel->setText("OK");
    RTCCalib=ui->rtcEdit->text().toInt(&ok,10);
}

void configWizard::on_pushButton_3_clicked()
{

    QString fileName = QFileDialog::getOpenFileName(this,tr("Save configuration file"),"C:\\",tr("INI File (*.ini)"));
    ui->configEdit->setText(fileName);
}

void configWizard::on_pushButton_2_clicked()
{
}

void configWizard::on_pushButton_4_clicked()
{
    if (ui->pushButton->text()=="Execute configuration"){

        ui->finalFrame->hide();
        ui->firstFrame->show();
        ui->pushButton->setText("Next");
        ui->pushButton_4->setEnabled(false);
    }
}

void configWizard::on_pushButton_clicked()
{
    ui->settingsList->clear();
    if (ui->pushButton->text()=="Next"){
        ui->finalFrame->show();
        ui->firstFrame->hide();
        ui->pushButton->setText("Execute configuration");
        ui->pushButton_4->setEnabled(true);
        QSettings settings(FileLoc,QSettings::IniFormat);

        ui->settingsList->appendPlainText("General settings:");
        ui->settingsList->appendPlainText(" -Name: "+ui->nameEdit->text());
        ui->settingsList->appendPlainText(" -Address: "+ui->addressEdit->text());
        ui->settingsList->appendPlainText(" -APN: "+settings.value("APN").toString());
        ui->settingsList->appendPlainText(" -IP: "+settings.value("IP").toString());
        ui->settingsList->appendPlainText(" -PORT: "+settings.value("PORT").toString());
        ui->settingsList->appendPlainText(" -Number: "+settings.value("NUMB").toString());

        ui->settingsList->appendPlainText("Primary configuration:");
        ui->settingsList->appendPlainText(tr(" -Primary battery threshold: %1 [V]").arg(settings.value("Primary_Bat_Threshold").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Primary report interval: %1").arg(settings.value("Primary_Report_Interval").toInt()));
        ui->settingsList->appendPlainText(tr(" -Primary measure interval: %1").arg(settings.value("Primary_Measure_Interval").toInt()));
        ui->settingsList->appendPlainText(tr(" -Primary average samples: %1").arg(settings.value("Primary_Average_Samples").toInt()));
        ui->settingsList->appendPlainText("Secondary configuration:");
        ui->settingsList->appendPlainText(tr(" -Secondary battery threshold: %1 [V]").arg(settings.value("Secondary_Bat_Threshold").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Secondary report interval: %1").arg(settings.value("Secondary_Report_Interval").toInt()));
        ui->settingsList->appendPlainText(tr(" -Secondary measure interval: %1").arg(settings.value("Secondary_Measure_Interval").toInt()));
        ui->settingsList->appendPlainText(tr(" -Secondary average samples: %1").arg(settings.value("Secondary_Average_Samples").toInt()));
        ui->settingsList->appendPlainText("Tertiary configuration:");
        ui->settingsList->appendPlainText(tr(" -Secondary battery threshold: %1 [V]").arg(settings.value("Tertiary_Bat_Threshold").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Secondary report interval: %1").arg(settings.value("Tertiary_Report_Interval").toInt()));
        ui->settingsList->appendPlainText(tr(" -Secondary measure interval: %1").arg(settings.value("Tertiary_Measure_Interval").toInt()));
        ui->settingsList->appendPlainText(tr(" -Secondary average samples: %1").arg(settings.value("Tertiary_Average_Samples").toInt()));

        switch(settings.value("AlarmReportSetting").toInt()){
            case(0):
                ui->settingsList->appendPlainText("Alarm report: By SMS");
                break;
            case(1):
                ui->settingsList->appendPlainText("Alarm report: By TCP");
                break;
        }

        ui->settingsList->appendPlainText("Voltage input 1 alarm settings:");
        switch(settings.value("VIn1_AlarmSetting").toInt()){
            case(0):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: OFF");
                break;
            case(1):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: MAX");
                break;
            case(2):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: MIN");
                break;
            case(3):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: MAX,MIN");
                break;
            case(4):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: DELTA");
                break;
            case(5):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: DELTA,MAX");
                break;
            case(6):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: DELTA,MIN");
                break;
            case(7):
                ui->settingsList->appendPlainText(" -VIn1 Alarm setting: DELTA,MAX,MIN");
                break;
        }
        ui->settingsList->appendPlainText(tr(" -VIn1 Maximum: %1 [V]").arg(settings.value("VIn1_Max").toDouble()));
        ui->settingsList->appendPlainText(tr(" -VIn1 Minimum: %1 [V]").arg(settings.value("VIn1_Min").toDouble()));
        ui->settingsList->appendPlainText(tr(" -VIn1 Delta: %1 [V]").arg(settings.value("VIn1_Delta").toDouble()));
        ui->settingsList->appendPlainText(tr(" -VIn1 Alarm timeout: %1").arg(settings.value("VIn1_Alarm_Timeout").toInt()));

        ui->settingsList->appendPlainText("Voltage input 2 alarm settings:");
        switch(settings.value("VIn2_AlarmSetting").toInt()){
            case(0):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: OFF");
                break;
            case(1):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: MAX");
                break;
            case(2):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: MIN");
                break;
            case(3):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: MAX,MIN");
                break;
            case(4):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: DELTA");
                break;
            case(5):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: DELTA,MAX");
                break;
            case(6):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: DELTA,MIN");
                break;
            case(7):
                ui->settingsList->appendPlainText(" -VIn2 Alarm setting: DELTA,MAX,MIN");
                break;
        }
        ui->settingsList->appendPlainText(tr(" -VIn2 Maximum: %1 [V]").arg(settings.value("VIn2_Max").toDouble()));
        ui->settingsList->appendPlainText(tr(" -VIn2 Minimum: %1 [V]").arg(settings.value("VIn2_Min").toDouble()));
        ui->settingsList->appendPlainText(tr(" -VIn2 Delta: %1 [V]").arg(settings.value("VIn2_Delta").toDouble()));
        ui->settingsList->appendPlainText(tr(" -VIn2 Alarm timeout: %1").arg(settings.value("VIn2_Alarm_Timeout").toInt()));

        ui->settingsList->appendPlainText("Current input 1 alarm settings:");
        switch(settings.value("IIn1_AlarmSetting").toInt()){
            case(0):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: OFF");
                break;
            case(1):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: MAX");
                break;
            case(2):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: MIN");
                break;
            case(3):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: MAX,MIN");
                break;
            case(4):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: DELTA");
                break;
            case(5):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: DELTA,MAX");
                break;
            case(6):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: DELTA,MIN");
                break;
            case(7):
                ui->settingsList->appendPlainText(" -IIn1 Alarm setting: DELTA,MAX,MIN");
                break;
        }
        ui->settingsList->appendPlainText(tr(" -IIn1 Maximum: %1 [V]").arg(settings.value("IIn1_Max").toDouble()));
        ui->settingsList->appendPlainText(tr(" -IIn1 Minimum: %1 [V]").arg(settings.value("IIn1_Min").toDouble()));
        ui->settingsList->appendPlainText(tr(" -IIn1 Delta: %1 [V]").arg(settings.value("IIn1_Delta").toDouble()));
        ui->settingsList->appendPlainText(tr(" -IIn1 Alarm timeout: %1").arg(settings.value("IIn1_Alarm_Timeout").toInt()));

        ui->settingsList->appendPlainText("Current input 2 alarm settings:");
        switch(settings.value("IIn2_AlarmSetting").toInt()){
            case(0):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: OFF");
                break;
            case(1):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: MAX");
                break;
            case(2):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: MIN");
                break;
            case(3):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: MAX,MIN");
                break;
            case(4):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: DELTA");
                break;
            case(5):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: DELTA,MAX");
                break;
            case(6):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: DELTA,MIN");
                break;
            case(7):
                ui->settingsList->appendPlainText(" -IIn2 Alarm setting: DELTA,MAX,MIN");
                break;
        }
        ui->settingsList->appendPlainText(tr(" -IIn2 Maximum: %1 [V]").arg(settings.value("IIn2_Max").toDouble()));
        ui->settingsList->appendPlainText(tr(" -IIn2 Minimum: %1 [V]").arg(settings.value("IIn2_Min").toDouble()));
        ui->settingsList->appendPlainText(tr(" -IIn2 Delta: %1 [V]").arg(settings.value("IIn2_Delta").toDouble()));
        ui->settingsList->appendPlainText(tr(" -IIn2 Alarm timeout: %1").arg(settings.value("IIn2_Alarm_Timeout").toInt()));

        ui->settingsList->appendPlainText("Temperature alarm settings:");
        switch(settings.value("Temp_AlarmSetting").toInt()){
        case(0):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: OFF");
            break;
        case(1):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: MAX");
            break;
        case(2):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: MIN");
            break;
        case(3):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: MAX,MIN");
            break;
        case(4):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: DELTA");
            break;
        case(5):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: DELTA,MAX");
            break;
        case(6):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: DELTA,MIN");
            break;
        case(7):
            ui->settingsList->appendPlainText(" -Temp Alarm setting: DELTA,MAX,MIN");
            break;
        }
        ui->settingsList->appendPlainText(tr(" -Temperature Maximum: %1 [C]").arg(settings.value("Temp_Max").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Temperature Minimum: %1 [C]").arg(settings.value("Temp_Min").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Temperature Delta: %1 [C]").arg(settings.value("Temp_Delta").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Temperature Alarm timeout: %1").arg(settings.value("Temp_Alarm_Timeout").toInt()));


        ui->settingsList->appendPlainText("Battery alarm settings:");
        switch(settings.value("Batt_AlarmSetting").toInt()){
            case(0):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: OFF");
                break;
            case(1):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: MAX");
                break;
            case(2):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: MIN");
                break;
            case(3):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: MAX,MIN");
                break;
            case(4):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: DELTA");
                break;
            case(5):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: DELTA,MAX");
                break;
            case(6):
                ui->settingsList->appendPlainText("Batt Alarm setting: DELTA,MIN");
                break;
            case(7):
                ui->settingsList->appendPlainText(" -Batt Alarm setting: DELTA,MAX,MIN");
                break;
            }
        ui->settingsList->appendPlainText(tr(" -Battery Maximum: %1 [V]").arg(settings.value("Batt_Max").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Battery Minimum: %1 [V]").arg(settings.value("Batt_Min").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Battery Delta: %1 [V]").arg(settings.value("Batt_Delta").toDouble()));
        ui->settingsList->appendPlainText(tr(" -Battery Alarm timeout: %1").arg(settings.value("Batt_Alarm_Timeout").toInt()));

        ui->settingsList->appendPlainText("Input ranges:");
        switch(settings.value("VIn1_ADS_GAIN").toInt()){
            case 0:
            ui->settingsList->appendPlainText(" -VIn1 range: 0-0.256 V");
            break;
            case 1:
            ui->settingsList->appendPlainText(" -VIn1 range: 0-0.512 V");
            break;
            case 2:
            ui->settingsList->appendPlainText(" -VIn1 range: 0-1.024 V");
            break;
            case 3:
            ui->settingsList->appendPlainText(" -VIn1 range: 0-2.048 V");
            break;
            case 4:
            ui->settingsList->appendPlainText(" -VIn1 range: 0-4.096 V");
            break;
            case 5:
            ui->settingsList->appendPlainText(" -VIn1 range: 0-6.144 V");
            break;

        }
        switch(settings.value("VIn2_ADS_GAIN").toInt()){
            case 0:
            ui->settingsList->appendPlainText(" -VIn2 range: 0-0.256 V");
            break;
            case 1:
            ui->settingsList->appendPlainText(" -VIn2 range: 0-0.512 V");
            break;
            case 2:
            ui->settingsList->appendPlainText(" -VIn2 range: 0-1.024 V");
            break;
            case 3:
            ui->settingsList->appendPlainText(" -VIn2 range: 0-2.048 V");
            break;
            case 4:
            ui->settingsList->appendPlainText(" -VIn2 range: 0-4.096 V");
            break;
            case 5:
            ui->settingsList->appendPlainText(" -VIn2 range: 0-6.144 V");
            break;

        }
        switch(settings.value("IIn1_ADS_GAIN").toInt()){
            case 0:
            ui->settingsList->appendPlainText(" -IIn1 range: 0-0.256 V");
            break;
            case 1:
            ui->settingsList->appendPlainText(" -IIn1 range: 0-0.512 V");
            break;
            case 2:
            ui->settingsList->appendPlainText(" -IIn1 range: 0-1.024 V");
            break;
            case 3:
            ui->settingsList->appendPlainText(" -IIn1 range: 0-2.048 V");
            break;
            case 4:
            ui->settingsList->appendPlainText(" -IIn1 range: 0-4.096 V");
            break;
            case 5:
            ui->settingsList->appendPlainText(" -IIn1 range: 0-6.144 V");
            break;

        }
        switch(settings.value("IIn2_ADS_GAIN").toInt()){
            case 0:
            ui->settingsList->appendPlainText(" -IIn2 range: 0-0.256 V");
            break;
            case 1:
            ui->settingsList->appendPlainText(" -IIn2 range: 0-0.512 V");
            break;
            case 2:
            ui->settingsList->appendPlainText(" -IIn2 range: 0-1.024 V");
            break;
            case 3:
            ui->settingsList->appendPlainText(" -IIn2 range: 0-2.048 V");
            break;
            case 4:
            ui->settingsList->appendPlainText(" -IIn2 range: 0-4.096 V");
            break;
            case 5:
            ui->settingsList->appendPlainText(" -IIn2 range: 0-6.144 V");
            break;

        }

        ui->settingsList->appendPlainText("Power options:");
        switch(settings.value("Power_Option").toInt()){
            case 0:
            ui->settingsList->appendPlainText(" -Sensor Power OFF");
            break;
            case 1:
            ui->settingsList->appendPlainText(" -Sensor Power A ON");
            break;
            case 2:
            ui->settingsList->appendPlainText(" -Sensor Power B ON");
            break;
            case 3:
            ui->settingsList->appendPlainText(" -Sensor Power A and B ON");
            break;
        }
        ui->settingsList->appendPlainText(tr(" -Power Wait time: %1 [ms]").arg(settings.value("Wait_Time").toInt()));


    }else{
        // Ovdje treba:

        // Upisat konfiguraciju u modul
        // Upisat RTC calib u modul
        // Konfigurirati SIM900
        // Poslati na server

        QSettings settings(FileLoc,QSettings::IniFormat);
        progress=new QProgressDialog("Configuration in progress->..", "Abort", 0, 120, this);
        progress->setWindowModality(Qt::WindowModal);
        progress->setMinimumDuration(1);
        progress->setAutoClose(false);
        int i=0;
        progress->setValue(i++);
        QDateTime T= QDateTime::currentDateTime();
        m_THController->SendDataToTH("CONFIG TIME SET "+T.toString("hh:mm:ss,dd/MM/yyyy")+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TIME)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG ADDR SET "+ui->addressEdit->text()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (ADDR)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PIN SET "+settings.value("PIN").toString()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG APN SET "+settings.value("APN").toString()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (APN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG IP SET "+settings.value("IP").toString()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (IP)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PORT SET "+settings.value("PORT").toString()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PORT)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG NAME SET "+ui->nameEdit->text()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (NAME)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG NUMB SET "+settings.value("NUMB").toString()+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (NUMB)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PLBT SET "+QString::number((unsigned short)(settings.value("Primary_Bat_Threshold").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PLBT)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PRI SET "+QString::number((unsigned short)settings.value("Primary_Report_Interval").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PRI)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PCI SET "+QString::number((unsigned short)settings.value("Primary_Measure_Interval").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PCI)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PAS SET "+QString::number((unsigned short)settings.value("Primary_Average_Samples").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PAS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG SLBT SET "+QString::number((unsigned short)(settings.value("Secondary_Bat_Threshold").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (SLBT)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG SRI SET "+QString::number((unsigned short)settings.value("Secondary_Report_Interval").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (SRI)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG SCI SET "+QString::number((unsigned short)settings.value("Secondary_Measure_Interval").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (SCI)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG SAS SET "+QString::number((unsigned short)settings.value("Secondary_Average_Samples").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (SAS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TLBT SET "+QString::number((unsigned short)(settings.value("Tertiary_Bat_Threshold").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TLBT)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TRI SET "+QString::number((unsigned short)settings.value("Tertiary_Report_Interval").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TRI)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TCI SET "+QString::number((unsigned short)settings.value("Tertiary_Measure_Interval").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TCI)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TAS SET "+QString::number((unsigned short)settings.value("Tertiary_Average_Samples").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TAS)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG ARO SET "+QString::number((unsigned short)settings.value("AlarmReportSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (ARO)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG V1AS SET "+QString::number((unsigned short)settings.value("VIn1_AlarmSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V1AS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V1MAX SET "+QString::number((unsigned short)(settings.value("VIn1_Max").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V1MAX)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V1MIN SET "+QString::number((unsigned short)(settings.value("VIn1_Min").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V1MIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V1DEL SET "+QString::number((unsigned short)(settings.value("VIn1_Delta").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V1DEL)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V1AT SET "+QString::number((unsigned short)settings.value("VIn1_Alarm_Timeout").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V1AT)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG V2AS SET "+QString::number((unsigned short)settings.value("VIn2_AlarmSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V2AS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V2MAX SET "+QString::number((unsigned short)(settings.value("VIn2_Max").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V2MAX)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V2MIN SET "+QString::number((unsigned short)(settings.value("VIn2_Min").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V2MIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V2DEL SET "+QString::number((unsigned short)(settings.value("VIn2_Delta").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V2DEL)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V2AT SET "+QString::number((unsigned short)settings.value("VIn2_Alarm_Timeout").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V2AT)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG I1AS SET "+QString::number((unsigned short)settings.value("IIn1_AlarmSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I1AS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I1MAX SET "+QString::number((unsigned short)(settings.value("IIn1_Max").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I1MAX)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I1MIN SET "+QString::number((unsigned short)(settings.value("IIn1_Min").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I1MIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I1DEL SET "+QString::number((unsigned short)(settings.value("IIn1_Delta").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I1DEL)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I1AT SET "+QString::number((unsigned short)settings.value("IIn1_Alarm_Timeout").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I1AT)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG I2AS SET "+QString::number((unsigned short)settings.value("IIn2_AlarmSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I2AS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I2MAX SET "+QString::number((unsigned short)(settings.value("IIn2_Max").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I2MAX)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I2MIN SET "+QString::number((unsigned short)(settings.value("IIn2_Min").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I2MIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I2DEL SET "+QString::number((unsigned short)(settings.value("IIn2_Delta").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I2DEL)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I2AT SET "+QString::number((unsigned short)settings.value("IIn2_Alarm_Timeout").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I2AT)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG TEAS SET "+QString::number((unsigned short)settings.value("Temp_AlarmSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TEAS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TEMAX SET "+QString::number((unsigned short)(settings.value("Temp_Max").toDouble()*1000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TEMAX)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TEMIN SET "+QString::number((unsigned short)(settings.value("Temp_Min").toDouble()*1000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TEMIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TEDEL SET "+QString::number((unsigned short)(settings.value("Temp_Delta").toDouble()*1000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TEDEL)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG TEAT SET "+QString::number((unsigned short)settings.value("Temp_Alarm_Timeout").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (TEAT)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG BAAS SET "+QString::number((unsigned short)settings.value("Batt_AlarmSetting").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (BAAS)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG BAMAX SET "+QString::number((unsigned short)(settings.value("Batt_Max").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (BAMAX)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG BAMIN SET "+QString::number((unsigned short)(settings.value("Batt_Min").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (BAMIN)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG BADEL SET "+QString::number((unsigned short)(settings.value("Batt_Delta").toDouble()*10000),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (BADEL)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG BAAT SET "+QString::number((unsigned short)settings.value("Batt_Alarm_Timeout").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (BAAT)");
        progress->setValue(i++);QThread::msleep(200);

        m_THController->SendDataToTH("CONFIG V1G SET "+QString::number((unsigned short)settings.value("VIn1_ADS_GAIN").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V1G)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG V2G SET "+QString::number((unsigned short)settings.value("VIn2_ADS_GAIN").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (V2G)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I1G SET "+QString::number((unsigned short)settings.value("IIn1_ADS_GAIN").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I1G)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG I2G SET "+QString::number((unsigned short)settings.value("IIn2_ADS_GAIN").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (I2G)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG PO SET "+QString::number((unsigned short)settings.value("Power_Option").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (PO)");
        progress->setValue(i++);QThread::msleep(200);
        m_THController->SendDataToTH("CONFIG WT SET "+QString::number((unsigned short)settings.value("Wait_Time").toInt(),16)+"#");
        progress->setValue(i++);QThread::msleep(200);
        progress->setLabelText("Module configuration in progress (WT)");
        progress->setValue(i++);QThread::msleep(200);


        m_THController->SendDataToTH("RTCCALIB SET "+QString::number(ui->rtcEdit->text().toInt(),16)+"#");
        QThread::msleep(200);
        progress->setLabelText("Module RTC configuration in progress");
        progress->setValue(i++);
        QThread::msleep(200);

        progress->setLabelText("Resetting measuring counter");
        QThread::msleep(200);
        m_THController->SendDataToTH("RESET MEASURE_COUNT#");
        QThread::msleep(200);
        progress->setLabelText("Resetting averaging counter");
        QThread::msleep(200);
        m_THController->SendDataToTH("RESET AVERAGE_COUNT#");
        QThread::msleep(200);
        progress->setLabelText("Resetting reporting counter");
        QThread::msleep(200);
        m_THController->SendDataToTH("RESET REPORT_COUNT#");
        QThread::msleep(200);
        progress->setLabelText("Modem configuration started\nPlease wait");
        QThread::msleep(200);
        m_THController->SendDataToTH("SIM900 INIT#");
        QThread::msleep(2000);

        if (ui->databaseWriteCheckbox->isChecked()){
            progress->setLabelText(tr("Connecting to server: %1 (%2)").arg(ui->IPLineEdit->text()).arg(ui->PortLineEdit->text()));


            _pSocket = new QTcpSocket(this); // <-- needs to be a member variable: QTcpSocket * _pSocket;

            QString IP=ui->IPLineEdit->text();
            int PORT=ui->PortLineEdit->text().toInt();
            QHostAddress addr(IP);
            _pSocket->connectToHost(addr,PORT);
            if( _pSocket->waitForConnected(30000)) {


                progress->setLabelText("Connected to server!");
                _pSocket->write(QString("[01"+ui->addressEdit->text()+"]\r\n").toLatin1());

                progress->setLabelText("Idenfying TH to server");
                m_configStrings.clear();
                m_configStrings.append(QString("[02,PIN SET "+settings.value("PIN").toString()+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,APN SET "+settings.value("APN").toString()+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,IP SET "+settings.value("IP").toString()+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,PORT SET "+settings.value("PORT").toString()+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,NAME SET "+ui->nameEdit->text()+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,NUMB SET "+settings.value("NUMB").toString()+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,PLBT SET "+QString::number((unsigned short)((double)settings.value("Primary_Bat_Threshold").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,PRI SET "+QString::number((unsigned short)settings.value("Primary_Report_Interval").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,PCI SET "+QString::number((unsigned short)settings.value("Primary_Measure_Interval").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,PAS SET "+QString::number((unsigned short)settings.value("Primary_Average_Samples").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,SLBT SET "+QString::number((unsigned short)((double)settings.value("Secondary_Bat_Threshold").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,SRI SET "+QString::number((unsigned short)settings.value("Secondary_Report_Interval").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,SCI SET "+QString::number((unsigned short)settings.value("Secondary_Measure_Interval").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,SAS SET "+QString::number((unsigned short)settings.value("Secondary_Average_Samples").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TLBT SET "+QString::number((unsigned short)((double)settings.value("Tertiary_Bat_Threshold").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TRI SET "+QString::number((unsigned short)settings.value("Tertiary_Report_Interval").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TCI SET "+QString::number((unsigned short)settings.value("Tertiary_Measure_Interval").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TAS SET "+QString::number((unsigned short)settings.value("Tertiary_Average_Samples").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,ARO SET "+QString::number((unsigned short)settings.value("AlarmReportSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V1AS SET "+QString::number((unsigned short)settings.value("VIn1_AlarmSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V1MAX SET "+QString::number((unsigned short)((double)settings.value("VIn1_Max").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V1MIN SET "+QString::number((unsigned short)((double)settings.value("VIn1_Min").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V1DEL SET "+QString::number((unsigned short)((double)settings.value("VIn1_Delta").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V1AT SET "+QString::number((unsigned short)settings.value("VIn1_Alarm_Timeout").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V2AS SET "+QString::number((unsigned short)settings.value("VIn2_AlarmSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V2MAX SET "+QString::number((unsigned short)((double)settings.value("VIn2_Max").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V2MIN SET "+QString::number((unsigned short)((double)settings.value("VIn2_Min").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V2DEL SET "+QString::number((unsigned short)((double)settings.value("VIn2_Delta").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V2AT SET "+QString::number((unsigned short)settings.value("VIn2_Alarm_Timeout").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I1AS SET "+QString::number((unsigned short)settings.value("IIn1_AlarmSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I1MAX SET "+QString::number((unsigned short)((double)settings.value("IIn1_Max").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I1MIN SET "+QString::number((unsigned short)((double)settings.value("IIn1_Min").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I1DEL SET "+QString::number((unsigned short)((double)settings.value("IIn1_Delta").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I1AT SET "+QString::number((unsigned short)settings.value("IIn1_Alarm_Timeout").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I2AS SET "+QString::number((unsigned short)settings.value("IIn2_AlarmSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I2MAX SET "+QString::number((unsigned short)((double)settings.value("IIn2_Max").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I2MIN SET "+QString::number((unsigned short)((double)settings.value("IIn2_Min").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I2DEL SET "+QString::number((unsigned short)((double)settings.value("IIn2_Delta").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I2AT SET "+QString::number((unsigned short)settings.value("IIn2_Alarm_Timeout").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TEAS SET "+QString::number((unsigned short)settings.value("Temp_AlarmSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TEMAX SET "+QString::number((unsigned short)((double)settings.value("Temp_Max").toDouble()*1000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TEMIN SET "+QString::number((unsigned short)((double)settings.value("Temp_Min").toDouble()*1000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TEDEL SET "+QString::number((unsigned short)((double)settings.value("Temp_Delta").toDouble()*1000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,TEAT SET "+QString::number((unsigned short)settings.value("Temp_Alarm_Timeout").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,BAAS SET "+QString::number((unsigned short)settings.value("Batt_AlarmSetting").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,BAMAX SET "+QString::number((unsigned short)((double)settings.value("Batt_Max").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,BAMIN SET "+QString::number((unsigned short)((double)settings.value("Batt_Min").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,BADEL SET "+QString::number((unsigned short)((double)settings.value("Batt_Delta").toDouble()*10000),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,BAAT SET "+QString::number((unsigned short)settings.value("Batt_Alarm_Timeout").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V1G SET "+QString::number((unsigned short)settings.value("VIn1_ADS_GAIN").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,V2G SET "+QString::number((unsigned short)settings.value("VIn2_ADS_GAIN").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I1G SET "+QString::number((unsigned short)settings.value("IIn1_ADS_GAIN").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,I2G SET "+QString::number((unsigned short)settings.value("IIn2_ADS_GAIN").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,PO SET "+QString::number((unsigned short)settings.value("Power_Option").toInt(),16)+"]\r\n\r\n").toLatin1());
                m_configStrings.append(QString("[02,WT SET "+QString::number((unsigned short)settings.value("Wait_Time").toInt(),16)+"]\r\n\r\n").toLatin1());

                m_timerCounter=0;
                clockTimer = new QTimer(this);
                connect(clockTimer, SIGNAL(timeout()), this, SLOT(sendConfig()));
                clockTimer->start(1000);

            }else{
                progress->setLabelText("Failed to connect to server!");
                int ret = QMessageBox::critical(this, tr("Error connecting to server"), "Please run the server application on selected IP and PORT",QMessageBox::Ok);

            }
        }else{
            int ret = QMessageBox::information(this, tr("Module configured"), "Module was properly configured",QMessageBox::Ok);

        }

        progress->close();
        this->close();
    }
}

void configWizard::on_cancelButton_clicked()
{
    this->close();
}

void configWizard::on_databaseWriteCheckbox_toggled(bool checked)
{
    if (checked){
        ui->IPLineEdit->setEnabled(true);
        ui->PortLineEdit->setEnabled(true);
    }else{
        ui->IPLineEdit->setEnabled(false);
        ui->PortLineEdit->setEnabled(false);
    }
}

void configWizard::sendConfig()
{
    _pSocket->write(m_configStrings.at(m_timerCounter).toLatin1());
    progress->setLabelText(tr("Writing parameter (%1) to database").arg(m_configStrings.at(m_timerCounter).split(" ").at(0).mid(3)));
    progress->setValue(progress->value()+1);

    if (m_timerCounter>m_configStrings.size()-2){
        int ret = QMessageBox::information(this, tr("Module configured"), "Module was properly configured and added to database",QMessageBox::Ok);
        _pSocket->disconnectFromHost();
        clockTimer->stop();
        m_timerCounter=0;
        this->close();

    }else{
        m_timerCounter++;
    }

}


void configWizard::THDataRecieved(QString Data){

}
