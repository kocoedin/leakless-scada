#include "getsetcombobox.h"

getSetComboBox::getSetComboBox (QString Command){
    m_CommandName=Command;
    this->addItem("NOTHING");
    this->addItem("SET");
    this->addItem("GET");
    connect(this, SIGNAL(currentIndexChanged(int)),this , SLOT(IndexChanged(int)));
}

getSetComboBox::~getSetComboBox(){

}

void getSetComboBox::IndexChanged(int CurrentIndex){
    emit GetSetIndexChanged(m_CommandName, CurrentIndex);
}

QString getSetComboBox::GetName(){
    return m_CommandName;
}
