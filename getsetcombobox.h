#ifndef GETSETCOMBOBOX_H
#define GETSETCOMBOBOX_H

#include <QComboBox>

class  getSetComboBox : public QComboBox
{
    Q_OBJECT

public:
    getSetComboBox (QString Command);
    ~getSetComboBox();
    QString GetName();

private slots:

    void IndexChanged(int CurrentIndex);
signals:

    void GetSetIndexChanged(QString Command, int CurrentIndex);
private:
    QString m_CommandName;
};

#endif // GETSETCOMBOBOX_H
