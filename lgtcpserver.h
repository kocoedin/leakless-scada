#ifndef LGTCPServer_H
#define LGTCPServer_H

#include <QObject>
#include <QStringList>
#include <QTCPServer>
#include <QAbstractSocket>
#include <QtGlobal>
#include <QTcpServer>
#include <QtNetwork>
#include "mysql.h"


/* Client handler */
class LGClientSocket : public QObject
{
    Q_OBJECT
public:
    typedef struct{
        QDateTime Timestamp;
        double V1;
        double Batt;
        bool   Valid;
    }LGMeasurementReturn;

    LGClientSocket(int socketDescriptor, QObject *parent, mySQL *database, QStringList CommandNames, QStringList CommandTypes);
    ~LGClientSocket();
    void startSocket();
    int  returnSocketDescriptor();
    void sendData(QByteArray Data);
    void FormulateQueryStrings();
    LGMeasurementReturn ParseMeasurementString(QString String);
    QDateTime GetPacketTime(QString TimeString);


    int         socketDescriptor;
    QString     text;
    QString     ClientAddress;
    QString     IPAddress;
    QDateTime   ConnectionTime;

signals:
    void errorSignal(QString ErrorInfo);
    void messageSignal(QString MessageInfo);
    void deleteRequest(int socketDescriptor);
    void dataRecieved(int socketDescriptor, QByteArray Data);
    void nodeGetSetResponded(QString Address);

public slots:
    void readyToRead();
    void stopSocket();
    void forceStopSocket();

private:


    QStringList     m_CommandNames;
    QStringList     m_CommandTypes;
    QTcpSocket      *tcpSocket;
    QTimer          *TimeoutTimer;
    bool            _connected;
    bool            _identified;
    mySQL           *m_Database;
    QStringList     QueryStrings;

};



/* Server */
class LGTCPServer : public QTcpServer
{
    Q_OBJECT



public:


    LGTCPServer(QObject *parent = 0, mySQL *database=0, QStringList CommandStrings=QStringList());
    ~LGTCPServer();

    QList<LGClientSocket *> *activeLGClientSocketList;
    QList<QHostAddress> returnIPs();
    void startServer(int port);
    void stopServer();



signals:
    void errorSignal(QString ErrorInfo);
    void messageSignal(QString MessageInfo);
    void nodeGetSetResponded(QString Address);

private slots:
    void errorSignalRecieved(QString ErrorInfo);
    void messageSignalRecieved(QString MessageInfo);
    void requestToDeleteHandler(int _socketDescriptor);

protected:
    void incomingConnection(int socketDescriptor);

private:

    void parseCommadStrings(QStringList CommandStrings);

    QStringList m_CommandNames;
    QStringList m_CommandTypes;
    QStringList fortunes;
    int _portChoice;
    QString _IPAddressChoice;
    mySQL *m_Database;
};




#endif // LGTCPServer_H
