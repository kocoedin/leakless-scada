#include "lgwidget.h"
#include "ui_lgwidget.h"

LGWidget::LGWidget(QMainWindow *parent, mySQL *Database,  MapWidget *Map) :
    QWidget(parent), ui(new Ui::LGWidget)
{
    QTreeWidgetItem *itemParent;
    QTreeWidgetItem *item;
    ListOfGetSetComboBoxes.clear();

    ui->setupUi(this);

    QTimer *timer=new QTimer();
    timer->setInterval(5000);
    timer->start();
    connect(timer,SIGNAL(timeout()),this, SLOT(showNodesOnMap()));

    this->setWindowTitle("Telemetry logger control");

    ui->connectionsListView->setContextMenuPolicy(Qt::CustomContextMenu);
    m_Map=Map;
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(THcontextMenu(QPoint)));

    QSettings settings("SCADAconfig.ini", QSettings::IniFormat);



    // Inicijalizacija server settingsa
    for (int i=0; i<ui->serverSettingsTreeWidget->topLevelItemCount();i++){
        itemParent = ui->serverSettingsTreeWidget->topLevelItem(i);

        if (itemParent->text(0)=="Server options"){
            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Port");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &ServerPort);
            ServerPort.setText(settings.value("LGServerSettings/Port").toString());


            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Report incoming connections");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &ReportIncomingConnections);
            if (settings.value("LGServerSettings/ReportIncomingConnections").toBool())ReportIncomingConnections.setChecked(true);
            else     ReportIncomingConnections.setChecked(false);

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Report incoming data");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &ReportIncomingData);
            if (settings.value("LGServerSettings/ReportIncomingData").toBool())ReportIncomingData.setChecked(true);
            else     ReportIncomingData.setChecked(false);

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Report outgoing data");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &ReportOutgoingData);
            if (settings.value("LGLGServerSettings/ReportOutgoingData").toBool())ReportOutgoingData.setChecked(true);
            else     ReportOutgoingData.setChecked(false);

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Report alarms");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &ReportAlarms);
            if (settings.value("LGServerSettings/ReportAlarms").toBool())ReportAlarms.setChecked(true);
            else     ReportAlarms.setChecked(false);

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Report parameter configuration");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &ReportParameterConfig);
            if (settings.value("LGServerSettings/ReportParameterConfiguration").toBool())ReportParameterConfig.setChecked(true);
            else     ReportParameterConfig.setChecked(false);
        }

        if (itemParent->text(0)=="Mail options"){
            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Mail");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &Mail);
            Mail.setText(settings.value("MailSettings/Mail").toString());

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Password");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &MailPassword);
            MailPassword.setText(settings.value("MailSettings/Password").toString());

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Service");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &MailService);
            MailService.setText(settings.value("MailSettings/Service").toString());

            item = new QTreeWidgetItem(itemParent);
            item->setText(0, "Port");
            ui->serverSettingsTreeWidget->setItemWidget(item, 1, &MailPort);
            MailPort.setText(settings.value("MailSettings/Port").toString());


        }
    }
    ui->serverSettingsTreeWidget->resizeColumnToContents(0);
    ui->serverSettingsTreeWidget->resizeColumnToContents(1);
    ui->serverSettingsTreeWidget->expandAll();


    ui->sensorInfoTreeWidget->resizeColumnToContents(0);


    itemParent = ui->measurementTree->topLevelItem(1);
    itemParent->setCheckState(0,Qt::Checked);
    // Inicijalizacija mjerenja
    ui->measurementTree->setItemWidget(itemParent, 3, &Batt_Unit);
    connect(&Batt_Unit,SIGNAL(textChanged(QString)),this, SLOT(Batt_Unit_changed(QString)));
    ui->measurementTree->setItemWidget(itemParent, 1, &Batt_Description);
    connect(&Batt_Description,SIGNAL(textChanged(QString)),this, SLOT(Batt_Description_changed(QString)));
    Batt_Scale.setMaximum(500.0);
    Batt_Scale.setMinimum(0);
    Batt_Scale.setSingleStep(0.01);
    Batt_Scale.setDecimals(3);
    ui->measurementTree->setItemWidget(itemParent, 2, &Batt_Scale);
    connect(&Batt_Scale,SIGNAL(valueChanged(double)),this, SLOT(Batt_Gain_changed(double)));

    itemParent = ui->measurementTree->topLevelItem(2);
    itemParent->setCheckState(0,Qt::Checked);
    // Inicijalizacija mjerenja
    ui->measurementTree->setItemWidget(itemParent, 3, &VIn1_Unit);
    connect(&VIn1_Unit,SIGNAL(textChanged(QString)),this, SLOT(VIn1_Unit_changed(QString)));
    ui->measurementTree->setItemWidget(itemParent, 1, &VIn1_Description);
    connect(&VIn1_Description,SIGNAL(textChanged(QString)),this, SLOT(VIn1_Description_changed(QString)));
    VIn1_Scale.setMaximum(500.0);
    VIn1_Scale.setMinimum(0);
    VIn1_Scale.setSingleStep(0.01);
    VIn1_Scale.setDecimals(3);
    ui->measurementTree->setItemWidget(itemParent, 2, &VIn1_Scale);
    connect(&VIn1_Scale,SIGNAL(valueChanged(double)),this, SLOT(VIn1_Gain_changed(double)));


    // Baza podataka
    database=Database;

    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->sensorInfoTreeWidget->resizeColumnToContents(0);
    ui->sensorInfoTreeWidget->expandAll();


    ui->measurementTree->resizeColumnToContents(0);
    ui->sensorInfoTreeWidget->resizeColumnToContents(0);

    ui->alarmListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    fillComboBox();
    refreshLGList();
    FindAllTreeItems(ui->sensorInfoTreeWidget->invisibleRootItem());

}

LGWidget::~LGWidget()
{
    ui->sensorInfoTreeWidget->clear();
    delete ui;
}


void LGWidget::StartLGServer(){

    QSettings settings("SCADAconfig.ini", QSettings::IniFormat);
    TcpServer=new LGTCPServer(this,database,CommandStrings);
    connect(TcpServer, SIGNAL(messageSignal(QString)),this,SLOT(messageSignalHandler(QString)));
    connect(TcpServer, SIGNAL(errorSignal(QString)),this,SLOT(errorSignalHandler(QString)));
    bool ok;
    settings.value("LGServerSettings/Port").toUInt(&ok);

    if (!ok) QMessageBox::critical(this,"Port configuration error!","The server cannot be started",QMessageBox::Ok);
    else  TcpServer->startServer(settings.value("LGServerSettings/Port").toUInt(&ok));
}

void LGWidget::errorSignalHandler(QString ErrorInfo){
    emit errorSignal(ErrorInfo);
}

void LGWidget::messageSignalHandler(QString MessageInfo){
    emit messageSignal(MessageInfo);
}

void LGWidget::FindAllTreeItems( QTreeWidgetItem *item )
{
    if (item->text(1)!=""){
        QStringList parameters=item->text(1).split(" ");
        Commands.append(parameters.at(0));
        item->setText(1,"");
        Items.append(item);

        if(parameters.at(1)=="ENUM"){
            QComboBox *comboBox=new QComboBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            comboBox->setProperty("Command",parameters.at(0));
            for (int i=2;i<parameters.count()-1;i++) comboBox->addItem(parameters.at(i));
            ui->sensorInfoTreeWidget->setItemWidget(item,1,comboBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            connect(comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(ComboBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="TF"){
            QCheckBox *checkBox=new QCheckBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            checkBox->setProperty("Command",parameters.at(0));
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, checkBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            connect(checkBox,SIGNAL(clicked(bool)),this,SLOT(CheckBox_changed(bool)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="DT"){
            QDateTimeEdit *dateTimeBox=new QDateTimeEdit();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            dateTimeBox->setProperty("Command",parameters.at(0));
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, dateTimeBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            connect(dateTimeBox,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(DateTimeBox_changed(QDateTime)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="STRING"){
            QLineEdit *textBox=new QLineEdit();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            textBox->setProperty("Command",parameters.at(0));
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, textBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            textBox->setMaxLength(parameters.at(3).toInt());
            if (parameters.last()!="RW") textBox->setEnabled(false);
            connect(textBox,SIGNAL(textChanged(QString)),this,SLOT(TextBox_changed(QString)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="UINT32"){
            QSpinBox *spinBox= new QSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",4);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, spinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="UINT16"){
            QSpinBox *spinBox= new QSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",2);
            spinBox->setProperty("Signed",false);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, spinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="UINT8"){
            QSpinBox *spinBox= new QSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",1);
            spinBox->setProperty("Signed",false);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, spinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="INT32"){
            QSpinBox *spinBox= new QSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",4);
            spinBox->setProperty("Signed",false);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, spinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="INT16"){
            QSpinBox *spinBox= new QSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",2);
            spinBox->setProperty("Signed",false);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, spinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="INT8"){
            QSpinBox *spinBox= new QSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",1);
            spinBox->setProperty("Signed",false);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, spinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="DOUBLE32"){
            QDoubleSpinBox *doubleSpinBox= new QDoubleSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            doubleSpinBox->setProperty("Command",parameters.at(0));
            doubleSpinBox->setProperty("Size",4);
            doubleSpinBox->setProperty("Multiplier",parameters.at(2));
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, doubleSpinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            doubleSpinBox->setMinimum(parameters.at(3).toDouble());
            doubleSpinBox->setMaximum(parameters.at(4).toDouble());
            connect(doubleSpinBox,SIGNAL(valueChanged(double)),this, SLOT(DoubleSpinBox_changed(double)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="DOUBLE16"){
            QDoubleSpinBox *doubleSpinBox= new QDoubleSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            doubleSpinBox->setProperty("Command",parameters.at(0));
            doubleSpinBox->setProperty("Size",2);
            doubleSpinBox->setProperty("Multiplier",parameters.at(2));
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, doubleSpinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            doubleSpinBox->setMinimum(parameters.at(3).toDouble());
            doubleSpinBox->setMaximum(parameters.at(4).toDouble());
            connect(doubleSpinBox,SIGNAL(valueChanged(double)),this, SLOT(DoubleSpinBox_changed(double)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="DOUBLE8"){
            QDoubleSpinBox *doubleSpinBox= new QDoubleSpinBox();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            doubleSpinBox->setProperty("Command",parameters.at(0));
            doubleSpinBox->setProperty("Size",1);
            doubleSpinBox->setProperty("Multiplier",parameters.at(2));
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, doubleSpinBox);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            doubleSpinBox->setMinimum(parameters.at(3).toDouble());
            doubleSpinBox->setMaximum(parameters.at(4).toDouble());
            connect(doubleSpinBox,SIGNAL(valueChanged(double)),this, SLOT(DoubleSpinBox_changed(double)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else if(parameters.at(1)=="DATETIME"){
            QDateTimeEdit *dateTimeEdit= new QDateTimeEdit();
            CommandStrings.append(item->text(1));
            getSetComboBox *getSetCombo=new getSetComboBox(parameters.at(0));
            ListOfGetSetComboBoxes.append(getSetCombo);
            dateTimeEdit->setProperty("Command",parameters.at(0));
            dateTimeEdit->setCalendarPopup(true);
            ui->sensorInfoTreeWidget->setItemWidget(item, 1, dateTimeEdit);
            ui->sensorInfoTreeWidget->setItemWidget(item,2,getSetCombo);
            connect(dateTimeEdit,SIGNAL(dateTimeChanged(QDateTime)),this, SLOT(DateTimeBox_changed(QDateTime)));
            connect(getSetCombo,SIGNAL(GetSetIndexChanged(QString,int)),this,SLOT(GetSetIndexChanged(QString,int)));
        }else{


        }

    }
    for( int i = 0; i < item->childCount(); ++i )FindAllTreeItems( item->child(i) );
}



void LGWidget::ComboBox_changed(int Value){
    QComboBox *obj  = dynamic_cast<QComboBox*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        database->queryModel->clear();
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]=%2 WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));

    }
}

void LGWidget::TextBox_changed(QString Value){
    QLineEdit *obj  = dynamic_cast<QLineEdit*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        database->queryModel->clear();
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]='%2' WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
    }
}

void LGWidget::CheckBox_changed(bool Value){
    QCheckBox *obj  = dynamic_cast<QCheckBox*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        if (Value){
            database->queryModel->clear();
            database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]=1 WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
        }
        else
        {
            database->queryModel->clear();
            database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]=0 WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
        }
    }
}

void LGWidget::DateTimeBox_changed(QDateTime Value){
    QDateTimeEdit *obj  = dynamic_cast<QDateTimeEdit*>(sender());

    QString DateValue = Value.toString("hh:mm:ss:dd:MM:yy");
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        database->queryModel->clear();
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]=%2 WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(DateValue).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
    }

}

void LGWidget::SpinBox_changed(int Value){
    unsigned int Number;
    Number=(unsigned int)(Value);
    QString HexValue;
    HexValue=QString::number((unsigned int)Number,16);
    QSpinBox *obj  = dynamic_cast<QSpinBox*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        database->queryModel->clear();
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]=%2 WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
    }
}

void LGWidget::DoubleSpinBox_changed(double Value){
    QDoubleSpinBox *obj  = dynamic_cast<QDoubleSpinBox*>(sender());
    int Multiplier=obj->property("Multiplier").toInt();
    unsigned long Number=Value*Multiplier;
    QString HexValue;
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        database->queryModel->clear();
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Rem_%1]=%2 WHERE Loc_Lat='%3' AND Loc_Long='%4';").arg(Command).arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
    }
}



void LGWidget::refreshLGList(){

    disconnect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(on_tableWidget_itemChanged(QTableWidgetItem*)));
    for (int k=0;ui->tableWidget->rowCount();k++){
        ui->tableWidget->removeRow(0);
    }
    int i=0;
    database->queryModel->clear();
    database->queryModel->setQuery(QString("SELECT Loc_Active, Loc_Lat, Loc_Long FROM LGParametri"));

    for(i =0; i<database->queryModel->rowCount();i++){
        QString LGLat, LGLong;
        LGLat=database->queryModel->record(i).value("Loc_Lat").toString();
        LGLong=database->queryModel->record(i).value("Loc_Long").toString();
        QTableWidgetItem *itemLat, *itemLong;
        itemLat  = new QTableWidgetItem(LGLat);
        itemLong = new QTableWidgetItem(LGLong);
        itemLat->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
        if (database->queryModel->record(i).value("Loc_Active").toBool()){
            itemLat->setCheckState(Qt::Checked);
        }else{
            itemLat->setCheckState(Qt::Unchecked);
        }
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        ui->tableWidget->setItem(i,0,itemLat);
        ui->tableWidget->setItem(i,1,itemLong);
    }
    connect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(on_tableWidget_itemChanged(QTableWidgetItem*)));
}



void LGWidget::on_tableWidget_itemChanged(QTableWidgetItem *item)
{
    if (item->column()==0){
        if (item->checkState()==Qt::Checked){
            database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_Active]=True WHERE  Loc_Lat='%1' AND Loc_Long='%2';").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
        }else{
            database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_Active]=False WHERE  Loc_Lat='%1' AND Loc_Long='%2';").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
        }
    }
}


void LGWidget::THConfigResponded(QString Lat, QString Long){
    if (Lat==CurrentSelectedLGLat && Long==CurrentSelectedLGLong)  updateNodeInfo(CurrentSelectedLGLat, CurrentSelectedLGLong);

}

void LGWidget::closeEvent(QCloseEvent *event){
    QMdiSubWindow *parent= qobject_cast<QMdiSubWindow *>(this->parent());
    parent->hide();

    event->ignore();
}

void LGWidget::showWidget(int TabNumber){
    QMdiSubWindow *parent= qobject_cast<QMdiSubWindow *>(this->parent());
    ui->tabWidget->setCurrentIndex(TabNumber);
    parent->show();
}

void LGWidget::showNodesOnMap(){
    QTimer* timer = dynamic_cast<QTimer*>(sender());
    if( timer != NULL )
    {
        int i=0;
        database->queryModel->clear();
        database->queryModel->setQuery(QString("SELECT Loc_Active, Loc_Lat, Loc_Long FROM LGParametri"));

        for(i =0; i<database->queryModel->rowCount();i++){
            QString LGLat, LGLong;
            LGLat=database->queryModel->record(i).value("Loc_Lat").toString();
            LGLong=database->queryModel->record(i).value("Loc_Long").toString();
            if (database->queryModel->record(i).value("Loc_Active").toBool()){
                m_Map->PutMarkerLG(LGLong.toFloat(),LGLat.toFloat(),"Logger No.:"+QString::number(i));
            }else{

            }
        }
    }
}

/* Ovo je za mjerenja */
void LGWidget::VIn1_Description_changed(QString Value){
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_V1Description]='%1' WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
}
void LGWidget::Batt_Description_changed(QString Value){
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_BattDescription]='%1' WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
}
void LGWidget::VIn1_Unit_changed(QString Value){
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_V1Unit]='%1' WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
}
void LGWidget::Batt_Unit_changed(QString Value){
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_BattUnit]='%1' WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
}
void LGWidget::VIn1_Gain_changed(double Value){
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_V1Gain]=%1 WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
}
void LGWidget::Batt_Gain_changed(double Value){
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_BattGain]=%1 WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(Value).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
}

void LGWidget::nodeSelected(int ID){
    if (ID==0)return;
}

void LGWidget::on_tableWidget_cellClicked(int row, int column)
{

    QString Lat, Long;
    Lat=ui->tableWidget->item(row,0)->text();
    Long=ui->tableWidget->item(row,1)->text();
    ui->Toolbox->setItemText(0,"Sensor configuration: " + Lat+","+Long);
    CurrentSelectedLGLat=Lat;
    CurrentSelectedLGLong=Long;

    updateNodeInfo(Lat, Long);
    ui->Toolbox->show();
}

void LGWidget::updateNodeInfo(QString Lat, QString Long)
{
    database->queryModel->clear();

    QSqlQueryModel *nodeInfo=database->queryModel;
    nodeInfo->clear();
    nodeInfo->setQuery(QString("SELECT * FROM LGParametri WHERE Loc_Lat='%1' AND Loc_Long='%2';").arg(Lat).arg(Long));
    if (nodeInfo->rowCount()==0){
        return;
    }
    QSqlRecord Record= nodeInfo->record(0);

    for (int i=0;i<Record.count();i++){
        QString FieldName= Record.fieldName(i);
        if (FieldName.startsWith("Rem_")){
            QString ParameterName = Record.fieldName(i).remove("Rem_");
            if (Commands.contains(ParameterName)){
                QTreeWidgetItem *item=Items.at(Commands.indexOf(ParameterName));

                bool ok;
                QWidget *widget=ui->sensorInfoTreeWidget->itemWidget(item,1);
                if(QComboBox *pb = qobject_cast<QComboBox *>(widget)) {
                    pb->setCurrentIndex(Record.value(FieldName).toInt());
                }else if(QSpinBox *pb = qobject_cast<QSpinBox *>(widget)){
                    pb->setValue(Record.value(FieldName).toInt());
                }else if(QDoubleSpinBox *pb = qobject_cast<QDoubleSpinBox *>(widget)){
                    pb->setValue(Record.value(FieldName).toDouble());
                }else if(QLineEdit *pb = qobject_cast<QLineEdit *>(widget)){
                    pb->setText(Record.value(FieldName).toString());
                }else if(QCheckBox *pb = qobject_cast<QCheckBox *>(widget)){
                    if(Record.value(FieldName).toBool()) pb->setChecked(true);
                    else pb->setChecked(false);
                }else if(QDateTimeEdit *pb = qobject_cast<QDateTimeEdit *>(widget)){
                    pb->setDateTime(QDateTime::fromString(Record.value(FieldName).toString(),"hh:mm:ss:dd:MM:yyyy"));
                }
            }
        }
    }

    //Time.setText(Record.value("LastResponse").toDateTime().toString("hh:mm:ss dd/MM/yyyy"));
    //QDateTime Expected=Record.value("LastResponse").toDateTime().addSecs(Primary_Report_Interval.value()*60);
    //ExpectedResponse.setText(Expected.toString("hh:mm:ss dd/MM/yyyy"));
    updateGetSet(Lat,Long);
    updateMeasurements(Lat,Long);
    updateAlarmList();
    updateEmailList();
}


void LGWidget::updateGetSet(QString Lat, QString Long)
{
    database->queryModel->clear();
    QSqlQueryModel *nodeInfo=database->queryModel;
    nodeInfo->setQuery(QString("SELECT SetGet_Query FROM LGParametri WHERE Loc_Lat='%1' AND Loc_Long='%2';").arg(Lat).arg(Long));
    QString QueryString=nodeInfo->record(0).value("SetGet_Query").toString();
    for (int i =0; i<ListOfGetSetComboBoxes.size();i++){
        if ( QueryString.contains(ListOfGetSetComboBoxes.at(i)->GetName()+" GET,")){
            ListOfGetSetComboBoxes.at(i)->setCurrentIndex(2);
        }else if ( QueryString.contains(ListOfGetSetComboBoxes.at(i)->GetName()+" SET,")){
            ListOfGetSetComboBoxes.at(i)->setCurrentIndex(1);
        }else{
            ListOfGetSetComboBoxes.at(i)->setCurrentIndex(0);
        }
    }
}

void LGWidget::updateMeasurements(QString Lat, QString Long)
{
    double Value;
    database->queryModel->clear();
    QSqlQueryModel *nodeInfo=database->queryModel;
    nodeInfo->setQuery(QString("SELECT * FROM LGParametri WHERE Loc_Lat='%1' AND Loc_Long='%2';").arg(Lat).arg(Long));
    QSqlRecord QueryRecord=nodeInfo->record(0);


    // Inicijalizacija mjerenja

    VIn1_Unit.setText(QueryRecord.value("Loc_V1Unit").toString());
    VIn1_Description.setText(QueryRecord.value("Loc_V1Description").toString());
    VIn1_Scale.setValue(QueryRecord.value("Loc_V1Gain").toDouble());

    Batt_Unit.setText(QueryRecord.value("Loc_BattUnit").toString());
    Batt_Description.setText(QueryRecord.value("Loc_BattDescription").toString());
    Batt_Scale.setValue(QueryRecord.value("Loc_BattGain").toDouble());

    ui->measurementTree->resizeColumnToContents(0);
    ui->measurementTree->resizeColumnToContents(1);
    ui->measurementTree->resizeColumnToContents(2);
    ui->measurementTree->resizeColumnToContents(3);

}

void LGWidget::on_dialDays_valueChanged(int value)
{
    if(value==1){
        ui->labelDays->setText(QString("1 day"));
    }else if(value==0){
        ui->labelDays->setText(QString("0 days"));
    }else if (value>1){
        ui->labelDays->setText(QString("%1 days").arg(value));
    }
}

void LGWidget::on_dialHours_valueChanged(int value)
{
    if(value==13){
        ui->labelHours->setText(QString("1 hour"));
    }else if(value>13){
        ui->labelHours->setText(QString("%1 hours").arg(value-12));
    }else if(value<13){
        ui->labelHours->setText(QString("%1 hours").arg(12+value));
    }
}

void LGWidget::fillComboBox(){
    QPixmap pixmap;
    QIcon Icon;

    ui->comboBox->clear();

    pixmap=QPixmap(":/icons/list-512.png"); Icon=QIcon(pixmap);
    ui->comboBox->addItem(Icon,"Select plotting option:");

    pixmap=QPixmap(":/icons/combo-512.png"); Icon=QIcon(pixmap);
    ui->comboBox->addItem(Icon, "Add to new plot");

    pixmap=QPixmap(":/icons/refresh-512_.png"); Icon=QIcon(pixmap);
    ui->comboBox->addItem(Icon, "Refresh opened plots");

//    for (int i=0;i<m_plottingTabWidget->count();i++){
//        pixmap=QPixmap(":/icons/rounded_rectangle-512.png"); Icon=QIcon(pixmap);
//        ui->comboBox->addItem(Icon,"Add to plot: "+m_plottingTabWidget->tabText(i));

//    }
}

void LGWidget::on_comboBox_currentIndexChanged(const QString &arg1)
{

    if (arg1.startsWith("Select plotting option",Qt::CaseSensitive)){

        return;
    }else if (arg1.startsWith("Add to new plot",Qt::CaseSensitive)){

        plotSelectedMeasurements(); // nacrtaj
        fillComboBox();

    }else if (arg1.startsWith("Refresh opened plots",Qt::CaseSensitive)){
        fillComboBox();
    }else{

//        int plotsFound=0;
//        for (int i=0;i<m_plottingTabWidget->count();i++){
//            QString tabName=m_plottingTabWidget->tabText(i);

//            if(arg1.endsWith(tabName)){
//                plotsFound++;


//                // Trazanje plotting widgeta u tabu
//                PlottingWidget* chosenPlottingWidget = NULL;

//                QWidget* pWidget= m_plottingTabWidget->widget(i);
//                QString className=pWidget->objectName();
//                if ( className== "plottingWidget"){
//                    chosenPlottingWidget = (PlottingWidget*)pWidget;
//                    chosenPlottingWidget->changeTitle("Test");
//                    plotSelectedMeasurements(chosenPlottingWidget);
//                    m_plottingTabWidget->setTabText(i,CurrentSelectedTHAddress+", "+m_plottingTabWidget->tabText(i));
//                }else{

//                }

                // ovdje dodajem plot na prvi koji se pojavio

            //}
       // }
    }



    ui->comboBox->setCurrentIndex(0);
}

void LGWidget::plotSelectedMeasurements()
{
    QVector<QString> measurementNames;
    QVector<double> measurementScaleValues;
    QDateTime startDate= QDateTime::currentDateTime();

    QDateTime endDate;
    int valueDays;
    int valueHours;
    QStringList linesInt;
    QRegExp rx("(\\d+)");


    PlottingWidget *chosenPlottingWidget = new PlottingWidget();

    int pos = 0;

    while ((pos = rx.indexIn(ui->labelDays->text(),pos)) != -1){
        linesInt << rx.cap(1);
        pos += rx.matchedLength();
    }

    valueDays=linesInt.at(0).toInt();
    linesInt.clear();pos=0;
    while ((pos = rx.indexIn(ui->labelHours->text(),pos)) != -1){
        linesInt << rx.cap(1);
        pos += rx.matchedLength();
    }
    valueHours=linesInt.at(0).toInt();
    QStringList AlarmTypes;
    QString QueryString="SELECT [GenerationTime]";
    // Dohvacanje imena mjerenja
    for(int i =0; i<ui->measurementTree->topLevelItemCount();i++){
        QTreeWidgetItem *item=ui->measurementTree->topLevelItem(i);
        if (item->checkState(0)==Qt::Checked){
            QString ItemName=item->text(0);
            if (ItemName=="Battery"){
                measurementNames.append(Batt_Description.text()+" ("+Batt_Unit.text()+")");
                AlarmTypes.append("Batt");
                measurementScaleValues.append(Batt_Scale.value());
                QueryString.append(QString(", [Batt]"));
            }
            if (ItemName=="Voltage Input 1"){
                measurementNames.append(VIn1_Description.text()+" ("+VIn1_Unit.text()+")");
                AlarmTypes.append("VIn1");
                measurementScaleValues.append(VIn1_Scale.value());
                QueryString.append(QString(", [VIN1]"));
            }
        }
    }

    QVector<QVector<QDateTime> > TimeStamps;
    QVector<QVector<double> >    Values;
    QVector<QString>             AlarmNames;
    QVector<QDateTime>           AlarmTimes;
    QList<QSqlRecord>            ModelRecords;
    QList<QSqlRecord>            AlarmRecords;

    endDate=startDate.addSecs(-1*(valueDays*24*60*60+valueHours*60*60));
    startDate= QDateTime::currentDateTime();
    QueryString.append(QString(" FROM LGMjerenja WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2' AND [GenerationTime] BETWEEN #%3# AND #%4#) ORDER BY [GenerationTime] ASC;").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong).arg(endDate.toString("MM/dd/yyyy hh:mm:ss")).arg(startDate.toString("MM/dd/yyyy hh:mm:ss")));
    database->queryModel->clear();
    database->queryModel->setQuery(QueryString);
    while (database->queryModel->canFetchMore()) database->queryModel->fetchMore();
    for (int i=0;i<database->queryModel->rowCount();i++){
        ModelRecords.append(database->queryModel->record(i));
    }
    int RowCount   = database->queryModel->rowCount();
    int ColumnCount= database->queryModel->columnCount();

    if (RowCount>=1){
        endDate=ModelRecords.at(0).value("GenerationTime").toDateTime();
        startDate=ModelRecords.at(RowCount-1).value("GenerationTime").toDateTime(); // Postavi pocetni i krajnji datum na podatke koje sam uspio preuzeti iz mjerenja
        QueryString=QString("SELECT * FROM LGAlarmi WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2' AND [GenerationTime] BETWEEN #%3# AND #%4#) ORDER BY [GenerationTime] ASC;").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong).arg(endDate.toString("MM/dd/yyyy hh:mm:ss"),startDate.toString("MM/dd/yyyy hh:mm:ss"));

        database->queryModel->clear();
        database->queryModel->setQuery(QueryString);

        for (int i=0;i<database->queryModel->rowCount();i++){
            AlarmRecords.append(database->queryModel->record(i));
        }

    }

    TimeStamps.resize(ColumnCount-1);
    Values.resize(ColumnCount-1);

    int i=0;
    for (int j=0; j<TimeStamps.count();j++){        // j mi broji po tipovima mjerenja
        for (i=0; i<ModelRecords.count();i++){
            TimeStamps[j].append( ModelRecords.at(i).value(0).toDateTime());
            Values[j].append( ModelRecords.at(i).value(j+1).toDouble()*measurementScaleValues.at(j));
        }
        // Ovdje ubacujem alarme

        for (int k=0; k<AlarmRecords.count();k++){
            QString AlarmNameString;
            if (AlarmRecords.at(k).value("ActiveAlarms").toString().contains(AlarmTypes.at(j)+"Max")){
                AlarmNameString.append("MAX\n");
            }
            if (AlarmRecords.at(k).value("ActiveAlarms").toString().contains(AlarmTypes.at(j)+"Min")){
                AlarmNameString.append("MIN\n");
            }
            if (AlarmRecords.at(k).value("ActiveAlarms").toString().contains(AlarmTypes.at(j)+"Del")){
                AlarmNameString.append("DEL\n");
            }
            if (!AlarmNameString.isEmpty()){
                AlarmNames.append(AlarmNameString);
                AlarmTimes.append(AlarmRecords.at(k).value("GenerationTime").toDateTime());
            }

        }



        if(i!=0){
            chosenPlottingWidget->addGraph(TimeStamps[j],Values[j],"",measurementNames.at(j),AlarmTimes,AlarmNames);
            emit plotRequest(chosenPlottingWidget);
        }
        AlarmTimes.clear();
        AlarmNames.clear();
    }

    if(i==0){
        ui->statusLabel->setText(QString("No data found"));
    }else{
        ui->statusLabel->setText(QString("%1 data entries found").arg(i));
    }
}


void LGWidget::GetSetIndexChanged(QString CommandName, int CurrentIndex){

    QSqlQueryModel *nodeInfo=database->queryModel;
    nodeInfo->setQuery(QString("SELECT SetGet_Query FROM LGParametri WHERE Loc_Lat='%1' AND Loc_Long='%2';").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
    if (nodeInfo->rowCount()==0){
        return;
    }
    QString QueryString=nodeInfo->record(0).value("SetGet_Query").toString();

    if (CurrentIndex==0){
        QueryString.remove(CommandName+" GET,");
        QueryString.remove(CommandName+" SET,");

    }else if (CurrentIndex==1){
        // SET
        QueryString.remove(CommandName + " GET,");
        QueryString.remove(CommandName + " SET,");
        QueryString.append(CommandName + " SET,");


    }else{
        // GET
        QueryString.remove(CommandName + " SET,");
        QueryString.remove(CommandName + " GET,");
        QueryString.append(CommandName + " GET,");
    }
    nodeInfo->setQuery(QString("UPDATE LGParametri SET [SetGet_Query]='%1' WHERE Loc_Lat='%2' AND Loc_Long='%3';").arg(QueryString).arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));

}

void LGWidget::on_dial_Days_Alarm_valueChanged(int value)
{
    if(value==5){
        ui->label_Days_Alarm->setText(QString("1 day"));
    }else if(value==4){
        ui->label_Days_Alarm->setText(QString("0 days"));
    }else if (value>5){
        ui->label_Days_Alarm->setText(QString("%1 days").arg(value-4));
    }else if (value<5){
        ui->label_Days_Alarm->setText(QString("%1 days").arg(4+value));

    }

    updateAlarmList();
}

void LGWidget::on_dial_Hours_Alarm_valueChanged(int value)
{
    if(value==13){
        ui->label_Hours_Alarm->setText(QString("1 hour"));
    }else if(value>13){
        ui->label_Hours_Alarm->setText(QString("%1 hours").arg(value-12));
    }else if(value<13){
        ui->label_Hours_Alarm->setText(QString("%1 hours").arg(12+value));
    }
    updateAlarmList();
}

void LGWidget::updateAlarmList()
{
    ui->alarmListWidget->clear();

    QDateTime startDate= QDateTime::currentDateTime();

    QDateTime endDate;
    int valueDays;
    int valueHours;
    QStringList linesInt;
    QRegExp rx("(\\d+)");

    int pos = 0;

    while ((pos = rx.indexIn(ui->label_Days_Alarm->text(),pos)) != -1){
        linesInt << rx.cap(1);
        pos += rx.matchedLength();
    }

    valueDays=linesInt.at(0).toInt();
    linesInt.clear();pos=0;
    while ((pos = rx.indexIn(ui->label_Hours_Alarm->text(),pos)) != -1){
        linesInt << rx.cap(1);
        pos += rx.matchedLength();
    }
    valueHours=linesInt.at(0).toInt();
    endDate=startDate.addSecs(-1*(valueDays*24*60*60+valueHours*60*60));


    QString QueryString=QString("SELECT * FROM LGAlarmi WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2' AND [GenerationTime] BETWEEN #%3# AND #%4#) ORDER BY [GenerationTime] ASC;").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong).arg(endDate.toString("MM/dd/yyyy hh:mm:ss"),startDate.toString("MM/dd/yyyy hh:mm:ss"));

    database->queryModel->clear();
    database->queryModel->setQuery(QueryString);

    for (int i=0; i<database->queryModel->rowCount();i++){
        QString AlarmLine;
        AlarmLine=QString (database->queryModel->record(i).value("GenerationTime").toDateTime().toString("(hh:mm:ss dd/MM/yyyy)")+" Alarms: "+database->queryModel->record(i).value("ActiveAlarms").toString());
        ui->alarmListWidget->addItem(AlarmLine);

    }

}

void LGWidget::updateEmailList()
{


    QString QueryString=QString("SELECT Loc_EMAIL, Loc_EMAIL_ACT FROM LGParametri WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2');").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong);
    database->queryModel->clear();
    database->queryModel->setQuery(QueryString);
    if (database->queryModel->rowCount()==0) return;
    QString EmailString=database->queryModel->record(0).value("Loc_EMAIL").toString();
    QStringList Emails=EmailString.split(",");
    ui->mailList->clear();
    ui->mailList->addItems(Emails);
    ui->mailCheckBox->setChecked(database->queryModel->record(0).value("Loc_EMAIL_ACT").toBool());
}

void LGWidget::on_alarmListWidget_customContextMenuRequested(const QPoint &pos)
{

    QListWidgetItem* temp = ui->alarmListWidget->itemAt(pos);
    if(temp == NULL) return;
    QString ItemName=ui->alarmListWidget->itemAt(pos)->text();
    QString DateTimeString=ItemName.replace("(","!").replace(")","!").split("!").at(1);
    QDateTime AlarmTime= QDateTime::fromString(DateTimeString,"hh:mm:ss dd/MM/yyyy");

    QMenu myMenu;
    myMenu.addAction("Remove alarm");
    // ...

    QPoint globalPos = ui->alarmListWidget->mapToGlobal(pos);
    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem)
    {
        QString QueryString = QString("DELETE * FROM LGAlarmi WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2' AND [GenerationTime]=%3)").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong).arg(AlarmTime.toString("#MM/dd/yyyy hh:mm:ss#"));
        database->queryModel->clear();
        database->queryModel->setQuery(QueryString);

        updateAlarmList();
    }
    else
    {
        // nothing was chosen
    }

}
void LGWidget::THcontextMenu(QPoint point)
{
    QTableWidgetItem *item=ui->tableWidget->itemAt(point);
    if (item!=NULL){
        int row=item->row();
        QString Lat=ui->tableWidget->item(row,0)->text();
        QString Long=ui->tableWidget->item(row,1)->text();
        QMenu menu(this);
        QAction *u1 = menu.addAction(tr("Remove LG"));
        QAction *u2 = menu.addAction(tr("Edit Lat Long"));
        QAction *a = menu.exec(ui->tableWidget->viewport()->mapToGlobal(point));
        if (a == u1)
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, "Delete LG", tr("Are you sure you want to delete LG(%1,%2)?").arg(Lat).arg(Long),
                                          QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes) {
                QString QueryString = QString("DELETE * FROM LGParametri WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2')").arg(Lat).arg(Long);
                database->queryModel->clear();
                database->queryModel->setQuery(QueryString);
                refreshLGList();
            } else {

            }
        }else if (a==u2){
            bool ok1,ok2;

            QString LatChoice=QInputDialog::getText(this,tr("Enter Latitude"),"Latitude:",QLineEdit::Normal,"47.0",&ok1);
            if (LatChoice=="" || !ok1) return;

            QString LongChoice=QInputDialog::getText(this,tr("Enter Longitude"),"Longitude:",QLineEdit::Normal,"47.0",&ok2);
            if (LongChoice=="" || !ok2) return;


            QString QStringLat=QString("UPDATE LGParametri SET [Loc_Lat]='%1',[Loc_Long]='%2' WHERE ([Loc_Lat]='%3' AND [Loc_Long]='%4')").arg(LatChoice).arg(LongChoice).arg(Lat).arg(Long);
            database->queryModel->clear();
            database->queryModel->setQuery(QStringLat);
            refreshLGList();

        }
    }else{

        QMenu menu(this);
        QAction *u1 = menu.addAction(tr("Add new LG (Not implemented)"));
        QAction *a = menu.exec(ui->tableWidget->viewport()->mapToGlobal(point));
        if (a == u1)
        {
        }
    }
}

void LGWidget::on_connectionsListView_customContextMenuRequested(const QPoint &pos)
{
    QTableWidgetItem* temp=ui->connectionsListView->itemAt(pos);
    if (temp!=NULL){
        QMenu *menu = new QMenu("Options:", NULL);
        menu->addAction("Remove connection");
        QAction *choice=menu->exec(ui->connectionsListView->mapToGlobal(pos));
        if (choice!=NULL){

        }
        delete menu;
    }else{

        QMenu *menu = new QMenu("Options:", NULL);
        menu->addAction("Refresh connections");
        QAction *choice=menu->exec(ui->connectionsListView->mapToGlobal(pos));
        if (choice!=NULL){
            if(choice->text()=="Refresh connections"){
                refreshActiveConnections();

            }
        }
        delete menu;
    }
}

void LGWidget::refreshActiveConnections(){
    ui->connectionsListView->clear();
    ui->connectionsListView->setRowCount(0);

    for (int i=0; i<TcpServer->activeLGClientSocketList->count();i++){
        if(TcpServer->activeLGClientSocketList->at(i)!=NULL){
            int currentRow=ui->connectionsListView->rowCount();
            ui->connectionsListView->setRowCount(currentRow+1);
            ui->connectionsListView->setItem(currentRow,0, new QTableWidgetItem(tr("%1").arg(TcpServer->activeLGClientSocketList->at(i)->socketDescriptor)));
            ui->connectionsListView->setItem(currentRow,1, new QTableWidgetItem(TcpServer->activeLGClientSocketList->at(i)->ClientAddress));
            ui->connectionsListView->setItem(currentRow,2, new QTableWidgetItem(TcpServer->activeLGClientSocketList->at(i)->IPAddress));
            ui->connectionsListView->setItem(currentRow,3, new QTableWidgetItem(TcpServer->activeLGClientSocketList->at(i)->ConnectionTime.toString("hh:mm:ss dd/MM/yyyy")));
        }

    }

}

void LGWidget::on_mailCheckBox_toggled(bool checked)
{
    database->queryModel->clear();
    if (checked){
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_EMAIL_ACT]=True WHERE [Loc_Lat]='%1' AND [Loc_Long]='%2'").arg(CurrentSelectedLGLat).arg(CurrentSelectedLGLong));
    }else{
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_EMAIL_ACT]=False WHERE [Loc_Lat]='%1' AND [Loc_Long]='%2'").arg(CurrentSelectedLGLong).arg(CurrentSelectedLGLong));

    }
}

void LGWidget::on_mailList_itemSelectionChanged()
{
    if (ui->mailList->currentItem()==NULL){
        ui->removeMailButton->setDisabled(true);
    }else{
        ui->removeMailButton->setEnabled(true);
    }
}

void LGWidget::on_addMailButton_clicked()
{
    bool ok;
    QString EMail = QInputDialog::getText(this, tr("Add new email address"),
                                             tr("Enter email:"), QLineEdit::Normal,
                                             QDir::home().dirName(), &ok);

    if (EMail=="" || !ok) return;


    QString QueryString=QString("SELECT Loc_EMAIL FROM LGParametri WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2');").arg(CurrentSelectedLGLong).arg(CurrentSelectedLGLong);
    database->queryModel->clear();
    database->queryModel->setQuery(QueryString);
    if (database->queryModel->rowCount()==0) return;
    QString EmailString=database->queryModel->record(0).value("Loc_EMAIL").toString();
    QStringList Emails=EmailString.split(",");
    if (Emails.contains(EMail)){
        return;
    }
    Emails.append(EMail);
    EmailString=Emails.join(",");
    database->queryModel->clear();
    database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_EMAIL]='%1' WHERE [Loc_Lat]='%2' AND [Loc_Long]='%3'").arg(EmailString).arg(CurrentSelectedLGLong).arg(CurrentSelectedLGLong));

    updateEmailList();
}

void LGWidget::on_removeMailButton_clicked()
{
    QString QueryString=QString("SELECT Loc_EMAIL FROM LGParametri WHERE ([Loc_Lat]='%1' AND [Loc_Long]='%2');").arg(CurrentSelectedLGLong).arg(CurrentSelectedLGLong);
    database->queryModel->clear();
    database->queryModel->setQuery(QueryString);
    if (database->queryModel->rowCount()==0) return;
    QString EmailString=database->queryModel->record(0).value("Loc_EMAIL").toString();
    QStringList Emails=EmailString.split(",");
    if (Emails.contains(ui->mailList->currentItem()->text())){
        Emails.removeAll(ui->mailList->currentItem()->text());
        EmailString=Emails.join(",");
        database->queryModel->clear();
        database->queryModel->setQuery(QString("UPDATE LGParametri SET [Loc_EMAIL]='%1' WHERE [Loc_Lat]='%1' AND [Loc_Long]='%2'").arg(EmailString).arg(CurrentSelectedLGLong).arg(CurrentSelectedLGLong));

        updateEmailList();
    }else{
        return;
    }

}

void LGWidget::on_SaveConfigurationButton_clicked()
{
    QSettings settings("SCADAconfig.ini", QSettings::IniFormat);

    settings.beginGroup("ServerSettings");
    settings.setValue("Port",ServerPort.text());
    settings.setValue("ReportIncomingConnections",ReportIncomingConnections.isChecked());
    settings.setValue("ReportIncomingData",ReportIncomingData.isChecked());
    settings.setValue("ReportOutgoingData",ReportOutgoingData.isChecked());
    settings.setValue("ReportAlarms",ReportAlarms.isChecked());
    settings.setValue("ReportParameterConfiguration",ReportParameterConfig.isChecked());
    settings.endGroup();

    settings.beginGroup("MailSettings");
    settings.setValue("Mail",Mail.text());
    settings.setValue("Password",MailPassword.text());
    settings.setValue("Service",MailService.text());
    settings.setValue("Port",MailPort.text());
    settings.endGroup();

    settings.sync();
    QMessageBox::information(this,"Configuration saved","Please restart SCADA application for setting to be accepted",QMessageBox::Ok);



}

void LGWidget::on_serwerPowerUp_clicked()
{
    TcpServer->startServer(ServerPort.text().toInt());
}

void LGWidget::on_serverPowerDown_clicked()
{
    TcpServer->stopServer();
}
