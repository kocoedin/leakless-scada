#ifndef LGWidget_H
#define LGWidget_H

#include <QTabWidget>
#include "plottingwidget.h"
#include "lgwidget.h"
#include "lgtcpserver.h"
#include "mapwidget.h"
#include "getsetcombobox.h"

namespace Ui {
class LGWidget;
}

class LGWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LGWidget(QMainWindow *parent = 0, mySQL *Database=0, MapWidget *Map=0);
    ~LGWidget();

    mySQL       *database;              // Pokazivac na bazu podatak
    void updateNodeInfo(QString Lat, QString Long);
    void updateGetSet(QString Lat, QString Long);
    void updateMeasurements(QString Lat, QString Long);
    void updateAlarmList();
    void updateEmailList();
    void fillComboBox();
    void refreshActiveConnections();
    void FindAllTreeItems(QTreeWidgetItem *item);
    void StartLGServer();

public slots:

    void THConfigResponded(QString Lat, QString Long);
    void closeEvent(QCloseEvent *event);
    void showWidget(int TabNumber);
    void showNodesOnMap();

private slots:

    void THcontextMenu(QPoint point);
    void GetSetIndexChanged(QString CommandName, int CurrentIndex);

    void on_tableWidget_cellClicked(int row, int column);
    void nodeSelected(int ID);
    void on_dialDays_valueChanged(int value);
    void on_dialHours_valueChanged(int value);
    void on_comboBox_currentIndexChanged(const QString &arg1);

    void plotSelectedMeasurements();


    /* Ovo je za mjerenja */
    void VIn1_Description_changed(QString Value);
    void Batt_Description_changed(QString Value);

    void VIn1_Unit_changed(QString Value);
    void Batt_Unit_changed(QString Value);

    void VIn1_Gain_changed(double Value);
    void Batt_Gain_changed(double Value);

    void on_dial_Days_Alarm_valueChanged(int value);
    void on_dial_Hours_Alarm_valueChanged(int value);

    void on_alarmListWidget_customContextMenuRequested(const QPoint &pos);
    void on_connectionsListView_customContextMenuRequested(const QPoint &pos);
    void on_tableWidget_itemChanged(QTableWidgetItem *item);

    void on_mailCheckBox_toggled(bool checked);
    void on_mailList_itemSelectionChanged();
    void on_addMailButton_clicked();
    void on_removeMailButton_clicked();

    void on_SaveConfigurationButton_clicked();
    void on_serwerPowerUp_clicked();
    void on_serverPowerDown_clicked();

    void ComboBox_changed(int Value);
    void TextBox_changed(QString Value);
    void CheckBox_changed(bool Value);
    void DateTimeBox_changed(QDateTime Value);
    void SpinBox_changed(int Value);
    void DoubleSpinBox_changed(double Value);

    void errorSignalHandler(QString ErrorInfo);
    void messageSignalHandler(QString MessageInfo);

signals:

    void serverPowerDownRequest();
    void serverPowerUpRequest();
    void plotRequest(PlottingWidget *chosenPlottingWidget);
    void errorSignal(QString ErrorInfo);
    void messageSignal(QString MessageInfo);

private:
    void refreshLGList();


    /* Ovo je za odabir mjerenja */
    QLineEdit VIn1_Description;
    QLineEdit VIn1_Unit;
    QDoubleSpinBox VIn1_Scale;
    QLineEdit Batt_Description;
    QLineEdit Batt_Unit;
    QDoubleSpinBox Batt_Scale;

    QList<getSetComboBox*> ListOfGetSetComboBoxes;


    // Server configuration postavke

    QLineEdit ServerPort;

    QLineEdit MailPort;
    QLineEdit MailPassword;
    QLineEdit MailService;
    QLineEdit Mail;

    QCheckBox ReportIncomingConnections;
    QCheckBox ReportIncomingData;
    QCheckBox ReportOutgoingData;
    QCheckBox ReportAlarms;
    QCheckBox ReportParameterConfig;

    QString CurrentSelectedLGLat;
    QString CurrentSelectedLGLong;

    Ui::LGWidget *ui;
    QString m_SensorType;
    MapWidget *m_Map;

    QStringList CommandStrings;

    LGTCPServer *TcpServer;

    QList<QString> Commands;
    QList<QTreeWidgetItem *> Items;

    bool m_pressedItemState;
};




#endif





