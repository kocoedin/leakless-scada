#include "logindialog.h"
#include "ui_logindialog.h"


LoginDialog::LoginDialog(QWidget *parent) :     QDialog(parent), ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    ui->TypeComboBox->addItem("Server user");
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::changeStatusBarText(QString text){

    ui->labelStatus->setText(text);
}
void LoginDialog::clearEntries(){
    ui->editUsername->setText("");
    ui->editPassword->setText("");
}
void LoginDialog::on_buttonConnect_clicked()
{
    emit connectButtonPressed(ui->editUsername->text(),ui->editPassword->text(),ui->TypeComboBox->currentIndex());
}

void LoginDialog::on_buttonClose_clicked()
{
    this->close();
    emit cancelButtonPressed();
}

void LoginDialog::on_LoginDialog_finished(int result)
{
    //if (ui->labelStatus->text().contains("Connected")) emit loginSucessfull(ui->editUsername->text());
    //else emit loginFailed(ui->editUsername->text());
}



void LoginDialog::on_editPassword_returnPressed()
{
    emit ui->buttonConnect->click();
}
