#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QDateTime>
#include "mysql.h"

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    void changeStatusBarText(QString text);
    void clearEntries();

private slots:
    void on_buttonConnect_clicked();
    void on_buttonClose_clicked();
    void on_LoginDialog_finished(int result);

    void on_editPassword_returnPressed();

signals:
    void connectButtonPressed(QString Username, QString Password, int Choice);
    void cancelButtonPressed();

private:
    Ui::LoginDialog *ui;
};

#endif // LOGINDIALOG_H
