#include "logwidget.h"
#include "ui_logwidget.h"

LogWidget::LogWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::LogWidget)
{
    ui->setupUi(this);

    QSettings settings("SCADAconfig.ini", QSettings::IniFormat);


    ReportIncomingConnections=settings.value("ServerSettings/ReportIncomingConnections").toBool();
    ReportIncomingData=settings.value("ServerSettings/ReportIncomingData").toBool();
    ReportOutgoingData=settings.value("ServerSettings/ReportOutgoingData").toBool();
    ReportAlarms=settings.value("ServerSettings/ReportAlarms").toBool();
    ReportParameterConfiguration=settings.value("ServerSettings/ReportParameterConfiguration").toBool();

    this->setMinimumHeight(100);
}

LogWidget::~LogWidget()
{
    delete ui;
}

void LogWidget::publishMessageToTerminal(QString message){
    if (message==" ") return;
    if (message.isEmpty()) return;
    QDateTime time(QDateTime::currentDateTime());

    //if (!ReportIncomingConnections && (message.contains("Client disconnected")||message.contains("Client connected")||message.contains("New client connecting..."))) return;
    //if (!ReportIncomingData && message.contains("Data recieved from")) return;
    //if (!ReportOutgoingData && message.contains("Data sent to client")) return;
    //if (!ReportParameterConfiguration && message.contains("changed parameter: ")) return;

    int fw = ui->messageWindow->fontWeight();
    QColor tc = ui->messageWindow->textColor();
    // append
    ui->messageWindow->setFontWeight( QFont::DemiBold );
    ui->messageWindow->setTextColor( QColor( "blue" ) );

    ui->messageWindow->append( time.toString("(hh:mm:ss) ")+message.remove("\n").remove("\r") );
    // restore
    ui->messageWindow->setFontWeight( fw );
    ui->messageWindow->setTextColor( tc );

    QScrollBar *sb = ui->messageWindow->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void LogWidget::publishErrorToTerminal(QString error){
    if (error==" ") return;
    if(error.isEmpty()) return;
    if (!ReportAlarms && error.contains("ALARM ON TH")) return;

    QDateTime time(QDateTime::currentDateTime());
    int fw = ui->messageWindow->fontWeight();
    QColor tc = ui->messageWindow->textColor();
    // append
    ui->messageWindow->setFontWeight( QFont::DemiBold );
    ui->messageWindow->setTextColor( QColor( "red" ) );
    ui->messageWindow->append( time.toString("(hh:mm:ss) ") + error.remove('\n').remove('\r') );
    // restore
    ui->messageWindow->setFontWeight( fw );
    ui->messageWindow->setTextColor( tc );

    QScrollBar *sb = ui->messageWindow->verticalScrollBar();
    sb->setValue(sb->maximum());

}
