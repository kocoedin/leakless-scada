#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QDockWidget>
#include <QDateTime>
#include <QWidget>
#include <QScrollBar>
#include <QMdiSubWindow>
#include <QSettings>

namespace Ui {
class LogWidget;
}

class LogWidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit LogWidget(QWidget *parent = 0);
    ~LogWidget();

public slots:
    void publishMessageToTerminal(QString message);
    void publishErrorToTerminal(QString error);

private:
    Ui::LogWidget *ui;
    bool ReportIncomingConnections;
    bool ReportIncomingData;
    bool ReportOutgoingData;
    bool ReportParameterConfiguration;
    bool ReportAlarms;

};

#endif // LOGWIDGET_H
