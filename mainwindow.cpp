#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->mdiArea);

    QSettings settings("SCADAconfig.ini", QSettings::IniFormat);

    MailString=settings.value("MailSettings/Mail").toString();
    MailPasswordString=settings.value("MailSettings/Password").toString();
    MailServiceString=settings.value("MailSettings/Service").toString();
    MailPortInt=settings.value("MailSettings/Port").toInt();


    this->showMaximized();

    createTrayActions();
    createTrayIcon();
    trayIcon->show();
    trayIcon->setToolTip("LeakLess SCADA by "+settings.value("Information/Manufacturer").toString());
    this->setWindowTitle("LeakLess SCADA by "+settings.value("Information/Manufacturer").toString());
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

    StatusBarUserFigure =  new QLabel();
    StatusBarUserSQL    =  new QLabel();
    StatusBarClock      =  new QLabel();
    StatusBarClock->setStyleSheet ("font: 14pt \"Arial Black\";");;

    ui->statusBar->addWidget(StatusBarClock);
    ui->statusBar->addWidget(StatusBarUserSQL);
    ui->statusBar->addWidget(StatusBarUserFigure);
    ui->statusBar->setLayoutDirection(Qt::RightToLeft);

    /* Log window */
    LogControllerWidget = new LogWidget();
    addDockWidget(Qt::BottomDockWidgetArea,LogControllerWidget);
    LogControllerWidget->show();

    /* Baza podataka */
    QString DatabaseLocation=settings.value("DatabaseSettings/DBLocation").toString();
    QString DatabaseName=settings.value("DatabaseSettings/DBConnectionString").toString();
    BazaPodataka = new mySQL(DatabaseLocation,DatabaseName);
    QString Return=BazaPodataka->Connect();
    publishErrorToTerminal(Return);

    /* Tooltipovi */
    StatusBarUserFigure->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Users/user/user-256.png")));;
    StatusBarUserFigure->setScaledContents(true);
    StatusBarUserFigure->setMaximumHeight(35);
    StatusBarUserFigure->setMaximumWidth(35);
    StatusBarUserSQL->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Data/mysql/mysql-256.png")));;
    StatusBarUserSQL->setScaledContents(true);
    StatusBarUserSQL->setMaximumHeight(35);
    StatusBarUserSQL->setMaximumWidth(35);
    StatusBarUserSQL->setToolTip("Connected to MySQL datbase:\n\nLocation: "+DatabaseLocation+"\nName: "+DatabaseName);

    /* Plotting widget dio */
    ui->actionMap_Show_Hide->setChecked(false);
    ui->actionPlot_Show_Hide->setChecked(true);
    ui->actionCommand_Show_Hide->setChecked(true);
    ui->actionCentral_Show_Hide->setChecked(true);

    /* Command list */
    CommandControllerWidget = new CommandWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea,CommandControllerWidget);
    connect(CommandControllerWidget,SIGNAL(commandItemClicked(QTreeWidgetItem*)), this, SLOT(changeSubjectTextAndLabel(QTreeWidgetItem*)));

    /* Mapa */
    ThMap=new MapWidget();
    addDockWidget(Qt::RightDockWidgetArea,ThMap);
    ui->actionMap_Show_Hide->setChecked(true);

    /* Mail server */
    //MailController = new SmtpController(MailString, MailPasswordString, MailServiceString, MailPortInt, 30000);

    /* Sat */
    QTimer *clockTimer = new QTimer(this);
    connect(clockTimer, SIGNAL(timeout()), this, SLOT(showTime()));
    clockTimer->start(1000);

    /* Remote TH widget */
    THControllerWidget=new THWidget(this,BazaPodataka,ThMap);
    ui->mdiArea->addSubWindow(THControllerWidget);
    connect(THControllerWidget,SIGNAL(plotRequest(PlottingWidget*)),this,SLOT(plotRequested(PlottingWidget*)));
    connect(THControllerWidget,SIGNAL(errorSignal(QString)),this,SLOT(publishErrorToTerminal(QString)));
    connect(THControllerWidget,SIGNAL(messageSignal(QString)),this,SLOT(publishMessageToTerminal(QString)));
    THControllerWidget->StartTHServer();

    /* Remote LG widget */
    LGControllerWidget=new LGWidget(this,BazaPodataka,ThMap);
    ui->mdiArea->addSubWindow(LGControllerWidget);
    connect(LGControllerWidget,SIGNAL(plotRequest(PlottingWidget*)),this,SLOT(plotRequested(PlottingWidget*)));
    connect(LGControllerWidget,SIGNAL(errorSignal(QString)),this,SLOT(publishErrorToTerminal(QString)));
    connect(LGControllerWidget,SIGNAL(messageSignal(QString)),this,SLOT(publishMessageToTerminal(QString)));
    LGControllerWidget->StartLGServer();

    /* Admin TH widget */
    AdminWidget=new adminTHwidget(this,BazaPodataka);
    ui->mdiArea->addSubWindow(AdminWidget);

    /* User control */
    userControl = new UserControlAndLogging(this, BazaPodataka);
    connect(userControl,SIGNAL(logoutSignal()),this,SLOT(loginFailed()));
    connect(userControl,SIGNAL(loginSignal()),this,SLOT(loginSucessfull()));
    connect(userControl->userLoginWindow,SIGNAL(rejected()),this,SLOT(loginFailed()));

    /* User login */
    loadLastLayout();
    userControl->RequestUserLogin();
    if(userControl->CurrentUser.logged_in==false){
        QApplication::quit();
        return;
    }
    publishMessageToTerminal("Logged in to server account");


    /* Web Server */

    //--docroot . --http-address 0.0.0.0 --http-port 8081
}

MainWindow::~MainWindow()
{
    delete ui;

}

void MainWindow::plotRequested(PlottingWidget *plottingWidget){

    QList<QDockWidget *> dockWidgets = findChildren<QDockWidget *>();

    addDockWidget(Qt::RightDockWidgetArea,plottingWidget);

    plottingWidget->show();
    foreach (QDockWidget *s, dockWidgets){
        PlottingWidget *foundPlot=qobject_cast<PlottingWidget*>(s);
        if (foundPlot){
            qDebug()<<"Plot Found";
            tabifyDockWidget(s,plottingWidget);
            return;
        }
    }
}

void MainWindow::loginSucessfull()
{
    QString PermissionsString= "User permissions:\n";
    PermissionsString.append(userControl->CurrentUser.Username+"\n");
    if (userControl->CurrentUser.sensor_permission_1==false && userControl->CurrentUser.sensor_permission_2==false) PermissionsString.append("\n-No sensor data permission");
    if (userControl->CurrentUser.sensor_permission_1==true) PermissionsString.append("\n-Level 1 sensor data permission");
    if (userControl->CurrentUser.sensor_permission_2==true) PermissionsString.append("\n-Level 2 sensor data permission");
    if (userControl->CurrentUser.control_permission_1==false && userControl->CurrentUser.control_permission_2==false) PermissionsString.append("\n-No control permission");
    if (userControl->CurrentUser.control_permission_1==true) PermissionsString.append("\n-Level 1 control permission");
    if (userControl->CurrentUser.control_permission_2==true) PermissionsString.append("\n-Level 2 control permission");
    if (userControl->CurrentUser.aditional_permission_1==false && userControl->CurrentUser.aditional_permission_2==false) PermissionsString.append("\n-No admin permission");
    if (userControl->CurrentUser.aditional_permission_1==true) PermissionsString.append("\n-Level 1 admin permission");
    if (userControl->CurrentUser.aditional_permission_2==true) PermissionsString.append("\n-Level 2 admin permission");
    StatusBarUserFigure->setToolTip(PermissionsString);
}

void MainWindow::loginFailed()
{
    if (userControl->CurrentUser.logged_in==false){
        BazaPodataka->disconnect();
        //userControl->RequestUserLogout();

        QApplication::quit();
        this->close();
    }
}

void MainWindow::on_actionExit_triggered()
{
    userControl->RequestUserLogout();
    loginFailed();
}

void MainWindow::on_actionLogout_triggered()
{
    userControl->RequestUserLogin();
}

void MainWindow::changeSubjectTextAndLabel(QTreeWidgetItem *item)
{
    QTreeWidgetItem *itemParent=item->parent();

    if (itemParent != NULL){
        if(itemParent->text(0).startsWith("Remote TH")){
            THControllerWidget->setFocus();
            if (item->text(0).startsWith("TH configuration")){
                THControllerWidget->showWidget(0);
            }else if  (item->text(0).startsWith("Server configuration")){
                THControllerWidget->showWidget(1);
            }
        }
        if(itemParent->text(0).startsWith("Remote LG")){
            LGControllerWidget->setFocus();
            if (item->text(0).startsWith("LG configuration")){
                LGControllerWidget->showWidget(0);
            }else if  (item->text(0).startsWith("Server configuration")){
                LGControllerWidget->showWidget(1);
            }
        }
        if(itemParent->text(0).startsWith("Admin settings")){
            AdminWidget->setFocus();
            if (item->text(0).startsWith("Database")){
                AdminWidget->showWidget(0);
            }else if  (item->text(0).startsWith("User")){
                AdminWidget->showWidget(1);
            }
        }
    }



}

void MainWindow::showTrayMessage(int Type,QString Title, QString Text, int duration)
{
    QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(Type);
    trayIcon->showMessage(Title, Text, icon, duration*1000);
}



void MainWindow::createTrayActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void MainWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(QIcon(":/icons/win8/PNG/It_Infrastructure/server/server-256.png"));
    trayIcon->setContextMenu(trayIconMenu);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible()) {
        showTrayMessage(1,"SCADA Monitor is running!", "Please close the program on system tray", 1000);
        event->ignore();
    }else{
        saveLastLayout();
    }
}
void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::DoubleClick:
        this->showMaximized();
        break;
    case QSystemTrayIcon::MiddleClick:
        showTrayMessage(1,"SCADA Monitor is running!", "Please close the program on system tray", 1000);
        break;
    default:
        ;
    }
}

void MainWindow::showTime()
{
    QDateTime time(QDateTime::currentDateTime());
    QString text = time.toString("   dd-MM-yyyy, hh:mm");
    StatusBarClock->setText(text);
}



void MainWindow::on_actionPlot_Show_Hide_triggered(bool checked)
{

//    if (checked==true){
//        ui->plottingTab->show();
//        ui->frame->show();
//    }else{
//        ui->plottingTab->hide();
//        if (ui->MapView->isHidden()) ui->frame->hide();
//    }
}

void MainWindow::on_actionMap_Show_Hide_triggered(bool checked)
{

//    if (checked==true){
//        ui->MapView->show();
//        ui->frame->show();
//    }else{
//        ui->MapView->hide();
//        if (ui->plottingTab->isHidden()) ui->frame->hide();
//    }
}

void MainWindow::on_actionCommand_Show_Hide_triggered(bool checked)
{
}

void MainWindow::on_actionCentral_Show_Hide_triggered(bool checked)
{

}


void MainWindow::publishMessageToTerminal(QString message){
    LogControllerWidget->publishMessageToTerminal(message);
}

void MainWindow::publishErrorToTerminal(QString error){
    LogControllerWidget->publishErrorToTerminal(error);
}

void MainWindow::on_actionHelp_contents_triggered()
{
    QStringList List;
    MailController->sendMail("alarmth@gmail.com","koco.edin@gmail.com","Test subject","Test body",List);
}


void MainWindow::saveLayout()
{
    QString fileName
        = QFileDialog::getSaveFileName(this, tr("Save layout"),"C:","Layout files (*.layout)");
    if (fileName.isEmpty())
        return;
    QFile file(fileName);
    LayoutFilePath=fileName;
    if (!file.open(QFile::WriteOnly)) {
        QString msg = tr("Failed to open %1\n%2")
                        .arg(fileName)
                        .arg(file.errorString());
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }

    QByteArray geo_data = saveGeometry();
    QByteArray layout_data = saveState();

    bool ok = file.putChar((uchar)geo_data.size());
    if (ok)
        ok = file.write(geo_data) == geo_data.size();
    if (ok)
        ok = file.write(layout_data) == layout_data.size();

    if (!ok) {
        QString msg = tr("Error writing to %1\n%2")
                        .arg(fileName)
                        .arg(file.errorString());
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }
}

void MainWindow::loadLayout()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load layout"),"C:","Layout files (*.layout)");
    if (fileName.isEmpty())
        return;
    QFile file(fileName);
    LayoutFilePath=fileName;
    if (!file.open(QFile::ReadOnly)) {
        QString msg = tr("Failed to open %1\n%2")
                        .arg(fileName)
                        .arg(file.errorString());
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }

    uchar geo_size;
    QByteArray geo_data;
    QByteArray layout_data;

    bool ok = file.getChar((char*)&geo_size);
    if (ok) {
        geo_data = file.read(geo_size);
        ok = geo_data.size() == geo_size;
    }
    if (ok) {
        layout_data = file.readAll();
        ok = layout_data.size() > 0;
    }

    if (ok)
        ok = restoreGeometry(geo_data);
    if (ok)
        ok = restoreState(layout_data);

    if (!ok) {
        QString msg = tr("Error reading %1")
                        .arg(fileName);
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }
}

void MainWindow::loadLastLayout()
{
    QString fileName = "autosave.layout";
    QFile file(fileName);
    if (!file.exists()) return;

    LayoutFilePath=fileName;
    if (!file.open(QFile::ReadOnly)) {
        QString msg = tr("Failed to open %1\n%2")
                        .arg(fileName)
                        .arg(file.errorString());
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }

    uchar geo_size;
    QByteArray geo_data;
    QByteArray layout_data;

    bool ok = file.getChar((char*)&geo_size);
    if (ok) {
        geo_data = file.read(geo_size);
        ok = geo_data.size() == geo_size;
    }
    if (ok) {
        layout_data = file.readAll();
        ok = layout_data.size() > 0;
    }

    if (ok)
        ok = restoreGeometry(geo_data);
    if (ok)
        ok = restoreState(layout_data);

    if (!ok) {
        QString msg = tr("Error reading %1")
                        .arg(fileName);
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }
}

void MainWindow::saveLastLayout()
{
    QString fileName= "autosave.layout";
    QFile file(fileName);
    LayoutFilePath=fileName;
    if (!file.open(QFile::WriteOnly)) {
        QString msg = tr("Failed to open %1\n%2")
                        .arg(fileName)
                        .arg(file.errorString());
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }

    QByteArray geo_data = saveGeometry();
    QByteArray layout_data = saveState();

    bool ok = file.putChar((uchar)geo_data.size());
    if (ok)
        ok = file.write(geo_data) == geo_data.size();
    if (ok)
        ok = file.write(layout_data) == layout_data.size();

    if (!ok) {
        QString msg = tr("Error writing to %1\n%2")
                        .arg(fileName)
                        .arg(file.errorString());
        QMessageBox::warning(this, tr("Error"), msg);
        return;
    }
}

void MainWindow::on_actionLoad_layout_triggered()
{
    loadLayout();
    ui->actionSave_layout->setEnabled(true);
}

void MainWindow::on_actionSave_layout_as_triggered()
{
    saveLayout();
    ui->actionSave_layout->setEnabled(true);
}

void MainWindow::on_actionOrganize_layout_triggered()
{
    ui->mdiArea->cascadeSubWindows();
}

void MainWindow::on_actionTitle_subwindows_triggered()
{
    ui->mdiArea->tileSubWindows();
}
