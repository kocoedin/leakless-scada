#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QTreeWidget>
#include <QDockWidget>
#include "mysql.h"
#include "logindialog.h"
#include "usercontrolandlogging.h"
#include "thwidget.h"
#include "lgwidget.h"
#include "plottingwidget.h"
#include "mapwidget.h"
#include "adminTHwidget.h"
#include "logwidget.h"
#include "commandwidget.h"
#include "smtp.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    mySQL *BazaPodataka;                // Referenca na objekt za komunikaciju sa bazom podataka
    UserControlAndLogging *userControl; // Opcije za upravljanje user accountovima
    Ui::MainWindow *ui;

    /* Tray funkcije */
    void showTrayMessage(int Type,QString Title, QString Text, int duration);
    void createTrayActions();
    void createTrayIcon();

private slots:
    void on_actionExit_triggered();
    void on_actionLogout_triggered();
    void closeEvent(QCloseEvent *event);// Promjena close metode da se nemoze zgasit osim iz traya

public slots:
    void loginSucessfull();
    void loginFailed();
    void changeSubjectTextAndLabel(QTreeWidgetItem *test);

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason);
    void publishMessageToTerminal(QString message);
    void publishErrorToTerminal(QString error);
    void showTime();

    void plotRequested(PlottingWidget *plottingWidget);

    void on_actionPlot_Show_Hide_triggered(bool checked);
    void on_actionMap_Show_Hide_triggered(bool checked);
    void on_actionCommand_Show_Hide_triggered(bool checked);
    void on_actionCentral_Show_Hide_triggered(bool checked);
    void on_actionHelp_contents_triggered();

    void saveLayout();
    void loadLayout();
    void loadLastLayout();
    void saveLastLayout();

    void on_actionLoad_layout_triggered();
    void on_actionSave_layout_as_triggered();
    void on_actionOrganize_layout_triggered();
    void on_actionTitle_subwindows_triggered();

private:
    QLabel                  *StatusBarUserFigure;
    QLabel                  *StatusBarUserSQL;
    QLabel                  *StatusBarClock;

    THWidget                *THControllerWidget;
    LGWidget                *LGControllerWidget;
    adminTHwidget           *AdminWidget;
    LogWidget               *LogControllerWidget;
    CommandWidget           *CommandControllerWidget;
    MapWidget               *ThMap;

    QSystemTrayIcon         *trayIcon;
    QMenu                   *trayIconMenu;
    QAction                 *minimizeAction;
    QAction                 *maximizeAction;
    QAction                 *restoreAction;
    QAction                 *quitAction;

    QTimeLine               *m_pTimeLine;
    SmtpController          *MailController;

    QString                 MailString;
    QString                 MailPasswordString;
    QString                 MailServiceString;
    int                     MailPortInt;

    QString                 LayoutFilePath;

};

#endif // MAINWINDOW_H
