#include "mapwidget.h"
#include "ui_mapwidget.h"

JavascriptObject::JavascriptObject(QObject *parent)
{
    _parent=parent;
    MapWidget *myMainWindow;
    myMainWindow = (MapWidget *)_parent;
}

QPointF JavascriptObject::getLatLng() {
    return mVar;
}

void JavascriptObject::set(float lat, float lng){
    MapWidget *myMainWindow;
    myMainWindow = (MapWidget *)_parent;
    mVar.setX(lat);
    mVar.setY(lng);
    myMainWindow->javascriptClick();
}




MapWidget::MapWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::MapWidget)
{
    ui->setupUi(this);
    qDebug()<< "try";
    QString content;
    QString fileName = ":/icons/index.html";
    QFile file(fileName);

    if( file.open(QFile::ReadOnly) ){
        content = QString(file.readAll());
    }else{
        //QMessageBox::information(this, tr("GPS Mapper"), tr("Cannot load %1.").arg(fileName));
        qFatal("Error opening index html file.");
    }
    file.close();

    ui->MapView->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical, Qt::ScrollBarAlwaysOff);
    ui->MapView->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
    ui->MapView->setHtml(content);
    myObject = new JavascriptObject(this);
    ui->MapView->page()->mainFrame()->addToJavaScriptWindowObject("myObject",myObject);

}

MapWidget::~MapWidget()
{
    delete ui;
}



void MapWidget::on_PutMarkerTH_clicked()
{
    QString marker = QString("markersth(%1,%2,'%3'); null")
            .arg(myObject->getLatLng().rx())
            .arg(myObject->getLatLng().ry()).arg(Message);

    ui->MapView->page()->mainFrame()->evaluateJavaScript(marker);
}

void MapWidget::on_PutMarkerLG_clicked()
{
    QString marker = QString("markerslg(%1,%2,'%3'); null")
            .arg(myObject->getLatLng().rx())
            .arg(myObject->getLatLng().ry()).arg(Message);

    ui->MapView->page()->mainFrame()->evaluateJavaScript(marker);
}

void MapWidget::PutMarkerTH(float Long, float Lat, QString message)
{
    Message=message;
    myObject->set(Lat,Long);
    //javascriptClick();
    on_PutMarkerTH_clicked();

}

void MapWidget::PutMarkerLG(float Long, float Lat, QString message)
{
    Message=message;
    myObject->set(Lat,Long);
    //javascriptClick();
    on_PutMarkerLG_clicked();

}

void MapWidget::on_ClearMarker_clicked()
{
    QString marker = "removemarkers(); null";
    ui->MapView->page()->mainFrame()->evaluateJavaScript(marker);
}

void MapWidget::javascriptClick(void){
    QString Lat=QString::number(myObject->getLatLng().rx());
    QString Long=QString::number(myObject->getLatLng().ry());
    emit THClicked(QString::number(myObject->getLatLng().rx()),QString::number(myObject->getLatLng().ry()));

}


void MapWidget::on_MapView_loadFinished(bool arg1)
{

}
