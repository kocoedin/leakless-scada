#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QDockWidget>
#include <QObject>
#include <QWebView>
#include <QPointF>
#include <QMainWindow>
#include <QWebFrame>
#include <QApplication>
#include <QtGui>
#include <QtWebKit>
#include <QFile>
#include <QMessageBox>

class JavascriptObject : public QObject
{
    Q_OBJECT

public:
    JavascriptObject(QObject *parent);
    QPointF getLatLng();

public slots:
   void set(float lat, float lng);

private:
   QPointF mVar;
   QObject *_parent;

};

namespace Ui {
class MapWidget;
}

class MapWidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit MapWidget(QWidget *parent = 0);
    ~MapWidget();

    void javascriptClick(void);
    JavascriptObject* myObject;
    void PutMarkerTH(float Long, float Lat, QString message);
    void PutMarkerLG(float Long, float Lat, QString message);


signals:
    void THClicked(QString Lat, QString Long);

private slots:
    void on_ClearMarker_clicked();
    void on_PutMarkerTH_clicked();
    void on_PutMarkerLG_clicked();

    void on_MapView_loadFinished(bool arg1);

private:
    QString Message;
    Ui::MapWidget *ui;
};

#endif // MAPWIDGET_H
