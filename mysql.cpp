#include "mysql.h"


mySQL::mySQL(QObject *parent) :
    QObject(parent)
{
}

mySQL::mySQL(QString DatabaseLocation, QString DatabaseName){
    databaseLocation = DatabaseLocation;
    databaseName = DatabaseName;
    database = QSqlDatabase::addDatabase("QODBC");
    queryModel = new QSqlQueryModel();
    database.setConnectOptions();
}

QString mySQL::Connect (){
    database.setDatabaseName(databaseName);
    bool Opened= database.open();
    if(Opened){
        return "Sucessfull";
    }else{
        database.close();
        return database.lastError().text();
    }
}
void mySQL::Disconnect (){
    database.close();

}

QString mySQL::Query(QString QueryString){
    queryModel->setQuery(QueryString);

    return queryModel->lastError().text();
}
