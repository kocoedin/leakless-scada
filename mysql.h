#ifndef MYSQL_H
#define MYSQL_H

#include <QObject>
#include <QtSql>
#include <QString>
#include <QtGui>
#include <QMainWindow>

class mySQL : public QObject
{
    Q_OBJECT

public:


    explicit mySQL(QObject *parent = 0);
    mySQL(QString DatabaseLocation, QString DatabaseName);

    QString Connect ();
    QString Query (QString QueryString);
    void Disconnect ();



    QSqlQueryModel *queryModel;
    QSqlDatabase database;

private:
    QString databaseLocation;
    QString databaseName;

    QString username;
    QString password;


signals:

public slots:

};

#endif // MYSQL_H
