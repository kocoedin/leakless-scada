#include "plottingwidget.h"
#include "ui_plottingwidget.h"

PlottingWidget::PlottingWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::PlottingWidget)
{
    srand(QDateTime::currentDateTime().toTime_t());
    ui->setupUi(this);

    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->customPlot->xAxis->rescale();
    ui->customPlot->yAxis->rescale();
    ui->customPlot->axisRect()->setupFullAxesBox();

    ui->customPlot->plotLayout()->insertRow(0);
    ui->customPlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->customPlot, "Plot"));

    ui->customPlot->xAxis->setLabel("Time");
    ui->customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    ui->customPlot->xAxis->setDateTimeFormat("MM-dd \n hh:mm:ss");

    ui->customPlot->yAxis->setLabel("Value");
    ui->customPlot->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(10);
    ui->customPlot->legend->setFont(legendFont);
    ui->customPlot->legend->setSelectedFont(legendFont);
    ui->customPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items

    connect(ui->customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));
    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
    connect(ui->customPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot, SIGNAL(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)), this, SLOT(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)));
    connect(ui->customPlot, SIGNAL(axisDoubleClick(QCPAxis*,QCPAxis::SelectablePart,QMouseEvent*)), this, SLOT(axisLabelDoubleClick(QCPAxis*,QCPAxis::SelectablePart)));
    connect(ui->customPlot, SIGNAL(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)), this, SLOT(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*)));
    connect(ui->customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*)));

    // setup policy and connect slot for context menu popup:
    ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));
    ui->customPlot->yAxis->rescale();
}


PlottingWidget::~PlottingWidget()
{
    delete ui;
}

void PlottingWidget::titleDoubleClick(QMouseEvent* event, QCPPlotTitle* title)
{
    Q_UNUSED(event)
    // Set the plot title by double clicking on it
    bool ok;
    QString newTitle = QInputDialog::getText(this, "Change plot title", "New plot title:", QLineEdit::Normal, title->text(), &ok);
    if (ok)
    {
        title->setText(newTitle);
        ui->customPlot->replot();
    }
}
void PlottingWidget::axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part)
{
    // Set an axis label by double clicking on it
    if (part == QCPAxis::spAxisLabel) // only react when the actual axis label is clicked, not tick label or axis backbone
    {
        bool ok;
        QString newLabel = QInputDialog::getText(this, "Change axis title", "New axis label:", QLineEdit::Normal, axis->label(), &ok);
        if (ok)
        {
            axis->setLabel(newLabel);
            ui->customPlot->replot();
        }
    }
}
void PlottingWidget::legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item)
{
    // Rename a graph by double clicking on its legend item
    Q_UNUSED(legend)
    if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
    {
        QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
        bool ok;
        QString newName = QInputDialog::getText(this, "QCustomPlot example", "New graph name:", QLineEdit::Normal, plItem->plottable()->name(), &ok);
        if (ok)
        {
            plItem->plottable()->setName(newName);
            ui->customPlot->replot();
        }
    }
}
void PlottingWidget::selectionChanged()
{
    /*
     normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
     the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
     and the axis base line together. However, the axis label shall be selectable individually.

     The selection state of the left and right axes shall be synchronized as well as the state of the
     bottom and top axes.

     Further, we want to synchronize the selection of the graphs with the selection state of the respective
     legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
     or on its legend item.
    */

    // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
    if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
            ui->customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
    {
        ui->customPlot->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
        ui->customPlot->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    }
    // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
    if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
            ui->customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
    {
        ui->customPlot->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
        ui->customPlot->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    }

    // synchronize selection of graphs with selection of corresponding legend items:
    for (int i=0; i<ui->customPlot->graphCount(); ++i)
    {
        QCPGraph *graph = ui->customPlot->graph(i);
        QCPPlottableLegendItem *item = ui->customPlot->legend->itemWithPlottable(graph);
        if (item->selected() || graph->selected())
        {
            item->setSelected(true);
            graph->setSelected(true);
        }
    }
}
void PlottingWidget::mousePress()
{
    // if an axis is selected, only allow the direction of that axis to be dragged
    // if no axis is selected, both directions may be dragged

    if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->xAxis->orientation());
    else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->yAxis->orientation());
    else
        ui->customPlot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}
void PlottingWidget::mouseWheel()
{
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeZoom(ui->customPlot->xAxis->orientation());
    else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeZoom(ui->customPlot->yAxis->orientation());
    else
        ui->customPlot->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

void PlottingWidget::addRandomGraph()
{
    int n = 50; // number of points in graph
    double xScale = (rand()/(double)RAND_MAX + 0.5)*2;
    double yScale = (rand()/(double)RAND_MAX + 0.5)*2;
    double xOffset = (rand()/(double)RAND_MAX - 0.5)*4;
    double yOffset = (rand()/(double)RAND_MAX - 0.5)*5;
    double r1 = (rand()/(double)RAND_MAX - 0.5)*2;
    double r2 = (rand()/(double)RAND_MAX - 0.5)*2;
    double r3 = (rand()/(double)RAND_MAX - 0.5)*2;
    double r4 = (rand()/(double)RAND_MAX - 0.5)*2;
    QVector<double> x(n), y(n);
    for (int i=0; i<n; i++)
    {
        x[i] = (i/(double)n-0.5)*10.0*xScale + xOffset;
        y[i] = (sin(x[i]*r1*5)*sin(cos(x[i]*r2)*r4*3)+r3*cos(sin(x[i])*r4*2))*yScale + yOffset;
    }

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("New graph %1").arg(ui->customPlot->graphCount()-1));
    ui->customPlot->graph()->setData(x, y);
    ui->customPlot->graph()->setLineStyle((QCPGraph::LineStyle)(rand()%5+1));
    if (rand()%100 > 75)  ui->customPlot->graph()->setScatterStyle(QCPScatterStyle((QCPScatterStyle::ScatterShape)(rand()%9+1)));
    QPen graphPen;
    graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
    graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->replot();

}
void PlottingWidget::addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit){

    if (time.count()!=valueDouble.count()) return;
    QVector<double> timeDouble;
    for (int i=0; i<time.count();i++) timeDouble.append(time[i].toTime_t());

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("%1 [%2]").arg(Name,Unit));
    ui->customPlot->graph()->setData(timeDouble, valueDouble);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsLine);
    ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssNone);

    QPen graphPen;
    graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
    graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->replot();
}
void PlottingWidget::addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit, QVector<QDateTime> AlarmTimes, QVector<QString> AlarmNames){

    if (time.count()!=valueDouble.count()) return;
    QVector<double> timeDouble;
    QVector<double> alarmTimeDouble;
    QVector<double> alarmValueDouble;
    for (int i=0; i<AlarmTimes.count();i++) {
        alarmTimeDouble.append(AlarmTimes[i].toTime_t());
    }

    for (int i=0; i<time.count();i++) {
        timeDouble.append(time[i].toTime_t());
    }
    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("%1 [%2]").arg(Name,Unit));
    ui->customPlot->graph()->setData(timeDouble, valueDouble);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsLine);
    ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssNone);

    QPen graphPen;
    graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
    graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->replot();

    // Ovdje crtam alarme
    for (int i=0;i<alarmTimeDouble.size();i++){

        // add the group velocity tracer (green circle):

        QCPItemTracer *groupTracer = new QCPItemTracer(ui->customPlot);
        ui->customPlot->addItem(groupTracer);
        groupTracer->setGraph(ui->customPlot->graph());
        groupTracer->setGraphKey(alarmTimeDouble.at(i));
        groupTracer->setInterpolating(true);
        groupTracer->setStyle(QCPItemTracer::tsCircle);
        groupTracer->setPen(QPen(Qt::red));
        groupTracer->setBrush(Qt::red);
        groupTracer->setSize(7);
        groupTracer->updatePosition();

        QCPItemText *phaseTracerText = new QCPItemText(ui->customPlot);
        ui->customPlot->addItem(phaseTracerText);
        phaseTracerText->position->setCoords(groupTracer->position->coords().rx(),groupTracer->position->coords().ry()); // lower right corner of axis rect
        phaseTracerText->setText(AlarmNames.at(i));
        phaseTracerText->setColor(Qt::red);
        phaseTracerText->setFont(QFont(font().family(), 12));
        phaseTracerText->setTextAlignment(Qt::AlignBottom);
    }
}

void PlottingWidget::changeTitle(QString Title){
    ui->customPlot->setWindowTitle(Title);

}

void PlottingWidget::removeSelectedGraph()
{
    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        ui->customPlot->removeGraph(ui->customPlot->selectedGraphs().first());
        ui->customPlot->replot();
    }
}



void PlottingWidget::processMenuChoice(QAction *ActionClicked)
{
    QCPScatterStyle scatterStyle=ui->customPlot->selectedGraphs().first()->scatterStyle();
    QPen plotPen=ui->customPlot->selectedGraphs().first()->pen();

    if(ActionClicked->text().startsWith("Plus", Qt::CaseInsensitive))              scatterStyle.setShape(QCPScatterStyle::ssPlus);
    else if(ActionClicked->text().startsWith("Dot", Qt::CaseInsensitive))          scatterStyle.setShape(QCPScatterStyle::ssDot);
    else if(ActionClicked->text().startsWith("Circle", Qt::CaseInsensitive))       scatterStyle.setShape(QCPScatterStyle::ssDisc);
    else if(ActionClicked->text().startsWith("Rectangle", Qt::CaseInsensitive))    scatterStyle.setShape(QCPScatterStyle::ssSquare);
    else if(ActionClicked->text().startsWith("Cross", Qt::CaseInsensitive))        scatterStyle.setShape(QCPScatterStyle::ssCross);
    else if(ActionClicked->text().startsWith("Star", Qt::CaseInsensitive))         scatterStyle.setShape(QCPScatterStyle::ssStar);
    else if(ActionClicked->text().startsWith("Triangle", Qt::CaseInsensitive))     scatterStyle.setShape(QCPScatterStyle::ssTriangle);
    else if(ActionClicked->text().startsWith("Diamond", Qt::CaseInsensitive))      scatterStyle.setShape(QCPScatterStyle::ssDiamond);

    else if(ActionClicked->text().endsWith("S 1", Qt::CaseInsensitive))            scatterStyle.setSize(1);
    else if(ActionClicked->text().endsWith("S 2", Qt::CaseInsensitive))            scatterStyle.setSize(2);
    else if(ActionClicked->text().endsWith("S 3", Qt::CaseInsensitive))            scatterStyle.setSize(4);
    else if(ActionClicked->text().endsWith("S 4", Qt::CaseInsensitive))            scatterStyle.setSize(6);
    else if(ActionClicked->text().endsWith("S 5", Qt::CaseInsensitive))            scatterStyle.setSize(8);
    else if(ActionClicked->text().endsWith("S 6", Qt::CaseInsensitive))            scatterStyle.setSize(10);
    else if(ActionClicked->text().endsWith("S 7", Qt::CaseInsensitive))            scatterStyle.setSize(12);
    else if(ActionClicked->text().endsWith("S 8", Qt::CaseInsensitive))            scatterStyle.setSize(14);
    else if(ActionClicked->text().endsWith("S 9", Qt::CaseInsensitive))            scatterStyle.setSize(16);

    else if(ActionClicked->text().startsWith("S Red", Qt::CaseInsensitive))        scatterStyle.pen().setColor(QColor(Qt::red));
    else if(ActionClicked->text().startsWith("S Blue", Qt::CaseInsensitive))       scatterStyle.pen().setColor(QColor(Qt::blue));
    else if(ActionClicked->text().startsWith("S Green", Qt::CaseInsensitive))      scatterStyle.pen().setColor(QColor(Qt::green));
    else if(ActionClicked->text().startsWith("S Black", Qt::CaseInsensitive))      scatterStyle.pen().setColor(QColor(Qt::black));
    else if(ActionClicked->text().startsWith("S Gray", Qt::CaseInsensitive))       scatterStyle.pen().setColor(QColor(Qt::gray));
    else if(ActionClicked->text().startsWith("S Yellow", Qt::CaseInsensitive))     scatterStyle.pen().setColor(QColor(Qt::yellow));
    else if(ActionClicked->text().startsWith("S Magenta", Qt::CaseInsensitive))    scatterStyle.pen().setColor(QColor(Qt::magenta));
    else if(ActionClicked->text().startsWith("S Cyan", Qt::CaseInsensitive))       scatterStyle.pen().setColor(QColor(Qt::cyan));

    else if(ActionClicked->text().startsWith("M Red", Qt::CaseInsensitive))        plotPen.setColor(Qt::red);
    else if(ActionClicked->text().startsWith("M Blue", Qt::CaseInsensitive))       plotPen.setColor(Qt::blue);
    else if(ActionClicked->text().startsWith("M Green", Qt::CaseInsensitive))      plotPen.setColor(Qt::green);
    else if(ActionClicked->text().startsWith("M Black", Qt::CaseInsensitive))      plotPen.setColor(Qt::black);
    else if(ActionClicked->text().startsWith("M Gray", Qt::CaseInsensitive))       plotPen.setColor(Qt::gray);
    else if(ActionClicked->text().startsWith("M Yellow", Qt::CaseInsensitive))     plotPen.setColor(Qt::yellow);
    else if(ActionClicked->text().startsWith("M Magenta", Qt::CaseInsensitive))    plotPen.setColor(Qt::magenta);
    else if(ActionClicked->text().startsWith("M Cyan", Qt::CaseInsensitive))       plotPen.setColor(Qt::cyan);

    else if(ActionClicked->text().endsWith("M 1", Qt::CaseInsensitive))            plotPen.setWidthF(1);
    else if(ActionClicked->text().endsWith("M 2", Qt::CaseInsensitive))            plotPen.setWidthF(2);
    else if(ActionClicked->text().endsWith("M 3", Qt::CaseInsensitive))            plotPen.setWidthF(4);
    else if(ActionClicked->text().endsWith("M 4", Qt::CaseInsensitive))            plotPen.setWidthF(6);
    else if(ActionClicked->text().endsWith("M 5", Qt::CaseInsensitive))            plotPen.setWidthF(8);
    else if(ActionClicked->text().endsWith("M 6", Qt::CaseInsensitive))            plotPen.setWidthF(10);
    else if(ActionClicked->text().endsWith("M 7", Qt::CaseInsensitive))            plotPen.setWidthF(12);
    else if(ActionClicked->text().endsWith("M 8", Qt::CaseInsensitive))            plotPen.setWidthF(14);
    else if(ActionClicked->text().endsWith("M 9", Qt::CaseInsensitive))            plotPen.setWidthF(16);



    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                graph->setScatterStyle(scatterStyle);
                graph->setPen(plotPen);
                if(ActionClicked->text().startsWith("Impulse", Qt::CaseInsensitive))    graph->setLineStyle(QCPGraph::lsImpulse);
                else if(ActionClicked->text().startsWith("Hold", Qt::CaseInsensitive))  graph->setLineStyle(QCPGraph::lsStepRight);
                else if(ActionClicked->text().startsWith("Linear", Qt::CaseInsensitive))graph->setLineStyle(QCPGraph::lsLine);
                else if(ActionClicked->text().startsWith("No", Qt::CaseInsensitive))    graph->setLineStyle(QCPGraph::lsNone);
            }
        }

        ui->customPlot->replot();
    }
}

void PlottingWidget::removeAllGraphs()
{
    ui->customPlot->clearGraphs();
    ui->customPlot->replot();
}
void PlottingWidget::contextMenuRequest(QPoint pos)
{
    QMenu menu;
    QMenu editLineWidthMenu;
    QMenu editLineStyleMenu;
    QMenu editMarkerMenu;
    QMenu editMarkerColorMenu;
    QMenu editMarkerWidthMenu;
    QMenu editMarkerStyleMenu;
    QMenu editLineColorMenu;
    QMenu editLineMenu;
    QPixmap pixmap;
    QIcon Icon;
    if (ui->customPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
    {
        menu.addAction("Move to top left");//, this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
        menu.addAction("Move to top center");//, this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
        menu.addAction("Move to top right");//, this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
        menu.addAction("Move to bottom right");//, this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
        menu.addAction("Move to bottom left");//, this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
    }
    else  // general context menu on graphs requested
    {
        if (ui->customPlot->selectedGraphs().size() > 0){
            menu.addAction("Remove selected graph");

            menu.addSeparator();
            menu.addMenu(&editLineMenu);
            pixmap=QPixmap(":/icons/line-512.png"); Icon=QIcon(pixmap);
            editLineMenu.setTitle("Line options");
            editLineMenu.setIcon(Icon);

            editLineMenu.addMenu(&editLineColorMenu);
            pixmap=QPixmap(":/icons/border_color-512.png"); Icon=QIcon(pixmap);

            editLineColorMenu.setIcon(Icon);
            editLineColorMenu.setTitle("Line color");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::red);      Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Red");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::blue);     Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Blue");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::green);    Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Green");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::yellow);   Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Yellow");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::cyan);     Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Cyan");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::magenta);  Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Magenta");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::black);    Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Black");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::gray);     Icon=QIcon(pixmap);
            editLineColorMenu.addAction(Icon,"M Gray");

            editLineMenu.addMenu(&editLineWidthMenu);
            pixmap=QPixmap(":/icons/line_width-512.png"); Icon=QIcon(pixmap);

            editLineWidthMenu.setIcon(Icon);
            editLineWidthMenu.setTitle("Line width");
            pixmap=QPixmap(":/icons/1-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 1");
            pixmap=QPixmap(":/icons/2-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 2");
            pixmap=QPixmap(":/icons/3-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 3");
            pixmap=QPixmap(":/icons/4-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 4");
            pixmap=QPixmap(":/icons/5-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 5");
            pixmap=QPixmap(":/icons/6-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 6");
            pixmap=QPixmap(":/icons/7-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 7");
            pixmap=QPixmap(":/icons/8-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 8");
            pixmap=QPixmap(":/icons/9-512.png"); Icon=QIcon(pixmap);
            editLineWidthMenu.addAction(Icon,"Size M 9");

            editLineMenu.addMenu(&editLineStyleMenu);
            pixmap=QPixmap(":/icons/scatter_plot-512_.png"); Icon=QIcon(pixmap);

            editLineStyleMenu.setIcon(Icon);
            editLineStyleMenu.setTitle("Line style");
            pixmap=QPixmap(":/icons/line_chart2-512.png"); Icon=QIcon(pixmap);
            editLineStyleMenu.addAction(Icon,"No line");
            pixmap=QPixmap(":/icons/line_chart1-512.png"); Icon=QIcon(pixmap);
            editLineStyleMenu.addAction(Icon,"Linear");
            pixmap=QPixmap(":/icons/line_chart3-512.png"); Icon=QIcon(pixmap);
            editLineStyleMenu.addAction(Icon,"Hold");
            pixmap=QPixmap(":/icons/line_chart4-512.png"); Icon=QIcon(pixmap);
            editLineStyleMenu.addAction(Icon,"Impulse");

            menu.addMenu(&editMarkerMenu);
            editMarkerMenu.setTitle("Marker options");
            pixmap=QPixmap(":/icons/marker_pen-512.png"); Icon=QIcon(pixmap);

            editMarkerMenu.setIcon(Icon);

            editMarkerMenu.addMenu(&editMarkerColorMenu);
            pixmap=QPixmap(":/icons/bg_color-512.png"); Icon=QIcon(pixmap);

            editMarkerColorMenu.setIcon(Icon);
            editMarkerColorMenu.setTitle("Marker color");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::red);      Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Red");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::blue);     Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Blue");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::green);    Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Green");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::yellow);   Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Yellow");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::cyan);     Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Cyan");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::magenta);  Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Magenta");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::black);    Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Black");
            pixmap=QPixmap(100,100); pixmap.fill(Qt::gray);     Icon=QIcon(pixmap);
            editMarkerColorMenu.addAction(Icon,"S Gray");


            editMarkerMenu.addMenu(&editMarkerWidthMenu);
            pixmap=QPixmap(":/icons/fit_to_width-512.png"); Icon=QIcon(pixmap);

            editMarkerWidthMenu.setIcon(Icon);
            editMarkerWidthMenu.setTitle("Marker size");
            pixmap=QPixmap(":/icons/1-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 1");
            pixmap=QPixmap(":/icons/2-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 2");
            pixmap=QPixmap(":/icons/3-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 3");
            pixmap=QPixmap(":/icons/4-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 4");
            pixmap=QPixmap(":/icons/5-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 5");
            pixmap=QPixmap(":/icons/6-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 6");
            pixmap=QPixmap(":/icons/7-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 7");
            pixmap=QPixmap(":/icons/8-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 8");
            pixmap=QPixmap(":/icons/9-512.png"); Icon=QIcon(pixmap);
            editMarkerWidthMenu.addAction(Icon,"Size S 9");

            editMarkerMenu.addMenu(&editMarkerStyleMenu);
            pixmap=QPixmap(":/icons/rounded_rectangle-512.png"); Icon=QIcon(pixmap);

            editMarkerStyleMenu.setIcon(Icon);
            editMarkerStyleMenu.setTitle("Marker shape");
            pixmap=QPixmap(":/icons/rectangle_stroked-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Rectangle");
            pixmap=QPixmap(":/icons/diamond-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Diamond");
            pixmap=QPixmap(":/icons/triangle_stroked-256.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Triangle");
            pixmap=QPixmap(":/icons/right_round-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Circle");
            pixmap=QPixmap(":/icons/X-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Cross");
            pixmap=QPixmap(":/icons/christmas_star-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Star");
            pixmap=QPixmap(":/icons/record-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Dot");
            pixmap=QPixmap(":/icons/plus2-512.png"); Icon=QIcon(pixmap);
            editMarkerStyleMenu.addAction(Icon,"Plus");


        }
        if (ui->customPlot->graphCount() > 0)            menu.addSeparator();


        pixmap=QPixmap(":/icons/remove_image-128.png"); Icon=QIcon(pixmap);
        menu.addAction(Icon,"Remove all graphs");

        pixmap=QPixmap(":/icons/resize-512.png"); Icon=QIcon(pixmap);
        menu.addAction(Icon,"Focus plot");

        pixmap=QPixmap(":/icons/pdf-512.png"); Icon=QIcon(pixmap);
        menu.addAction(Icon,"Save to PDF");

        pixmap=QPixmap(":/icons/png-512.png"); Icon=QIcon(pixmap);
        menu.addAction(Icon,"Save to PNG");

    }

    QAction *Choice=menu.exec(ui->customPlot->mapToGlobal(pos));
    if(Choice==NULL) return;
    if (Choice->text().startsWith("Save")){
        saveFigure(Choice);
    }else if (Choice->text().startsWith("Focus")){
        adjustAxes();
    }else if (Choice->text().startsWith("Remove all")){
        removeAllGraphs();
    }else if (Choice->text().startsWith("Remove selected")){
        removeSelectedGraph();
    }else if (Choice->text().startsWith("Move to")){
        if (Choice->text().endsWith("top left")){
            moveLegend((int)(Qt::AlignTop|Qt::AlignLeft));
        }else if (Choice->text().endsWith("top right")){
            moveLegend((int)(Qt::AlignTop|Qt::AlignRight));

        }else if (Choice->text().endsWith("top center")){
            moveLegend((int)(Qt::AlignTop|Qt::AlignCenter));

        }else if (Choice->text().endsWith("bottom left")){
            moveLegend((int)(Qt::AlignBottom|Qt::AlignLeft));

        }else if (Choice->text().endsWith("bottom right")){
            moveLegend((int)(Qt::AlignBottom|Qt::AlignRight));

        }
    }else{
        processMenuChoice(Choice);
    }
}
void PlottingWidget::saveFigure(QAction *Action)
{
    if (Action->text()=="Save to PDF"){

        QString filename = QInputDialog::getText(this, "Save figure to PDF", "File name:", QLineEdit::Normal);
        ui->customPlot->savePdf(filename+".pdf",false,1600,900);
    }
    if (Action->text()=="Save to PNG"){
        QString filename = QInputDialog::getText(this, "Save figure to PNG", "File name:", QLineEdit::Normal);
        ui->customPlot->savePng(filename+".png",1600,900,1);
    }
}
void PlottingWidget::adjustAxes()
{
    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                ui->customPlot->graph(i)->rescaleAxes();
            }
        }

        ui->customPlot->replot();
    }else{
        ui->customPlot->rescaleAxes();
        ui->customPlot->replot();
    }
}
void PlottingWidget::moveLegend()
{
    if (QAction* contextAction = qobject_cast<QAction*>(sender())) // make sure this slot is really called by a context menu action, so it carries the data we need
    {
        bool ok;
        int dataInt = contextAction->data().toInt(&ok);
        if (ok)
        {
            ui->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
            ui->customPlot->replot();
        }
    }
}
void PlottingWidget::moveLegend(int dataInt)
{

        bool ok;
        if (ok)
        {
            ui->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
            ui->customPlot->replot();
        }
}
void PlottingWidget::graphClicked(QCPAbstractPlottable *plottable)
{

    //ui->statusBar->showMessage(QString("Clicked on graph '%1'.").arg(plottable->name()), 1000);
}
