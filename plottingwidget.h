#ifndef PlottingWidget_H
#define PlottingWidget_H

#include <QDockWidget>
#include "qcustomplot.h"

namespace Ui {
class PlottingWidget;
}

class PlottingWidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit PlottingWidget(QWidget *parent = 0);
    ~PlottingWidget();
    void addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit);
    void addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit, QVector<QDateTime> AlarmTimes, QVector<QString> AlarmNames);
    void changeTitle(QString Title);
public slots:
    void addRandomGraph();


private slots:
    void titleDoubleClick(QMouseEvent *event, QCPPlotTitle *title);
    void axisLabelDoubleClick(QCPAxis* axis, QCPAxis::SelectablePart part);
    void legendDoubleClick(QCPLegend* legend, QCPAbstractLegendItem* item);
    void selectionChanged();
    void mousePress();
    void mouseWheel();

    void removeSelectedGraph();
    void removeAllGraphs();

    void contextMenuRequest(QPoint pos);
    void moveLegend();
    void moveLegend(int dataInt);
    void graphClicked(QCPAbstractPlottable *plottable);
    void processMenuChoice(QAction *ActionClicked);
    void saveFigure(QAction *Action);
    void adjustAxes();

private:
    Ui::PlottingWidget *ui;
};


#endif // PlottingWidget_H
