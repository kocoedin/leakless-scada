#include "thtcpserver.h"


//! [0]
THClientSocket::THClientSocket(int socketDescriptor, QObject *parent, mySQL *database, QStringList CommandNames, QStringList CommandTypes)
    : QObject(parent), socketDescriptor(socketDescriptor)
{
    _connected=false;
    _identified=false;
    m_CommandNames=CommandNames;
    m_CommandTypes=CommandTypes;

    ClientAddress="Unknown";
    m_Database=database;
    QueryStrings.clear();
    TimeoutTimer=new QTimer();
    connect(TimeoutTimer,SIGNAL(timeout()),this, SLOT(forceStopSocket()));
    TimeoutTimer->start(30000);
}

THClientSocket::~THClientSocket()
{
    _connected=false;
    delete tcpSocket;
    delete TimeoutTimer;
}


void THClientSocket::startSocket()
{
    tcpSocket=new QTcpSocket();
    if (!tcpSocket->setSocketDescriptor(socketDescriptor)) {
        emit deleteRequest(socketDescriptor);
        emit messageSignal(tr("TH client connection error: %1").arg(tcpSocket->errorString()));
        return;
    }
    connect(tcpSocket, SIGNAL(readyRead())   ,this, SLOT(readyToRead()));
    connect(tcpSocket, SIGNAL(disconnected()),this, SLOT(stopSocket()));
    emit messageSignal(tr("TH client connected(%1): %2").arg(socketDescriptor).arg(tcpSocket->peerAddress().toString()));
    ConnectionTime=QDateTime::currentDateTime();
    IPAddress=tcpSocket->peerAddress().toString();
    _connected=true;
    // Potrebno je identificirati klijenta

}

void THClientSocket::sendData(QByteArray Data){

    tcpSocket->write(Data);
    if(tcpSocket->waitForBytesWritten()){
        emit messageSignal(tr("Data sent to TH client(%1): ").arg(ClientAddress)+QString(Data));
    }else{
        emit errorSignal(tr("Data sent to TH client failed(%1)").arg(ClientAddress));
    }
}

void THClientSocket::stopSocket()
{
    disconnect(TimeoutTimer,SIGNAL(timeout()),this, SLOT(stopSocket()));
    disconnect(tcpSocket, SIGNAL(disconnected()),this, SLOT(stopSocket()));
    emit messageSignal(tr("TH client disconnected(%1): %2").arg(socketDescriptor).arg(tcpSocket->peerAddress().toString()));

    TimeoutTimer->stop();
    tcpSocket->disconnectFromHost();
    tcpSocket->waitForDisconnected(10000);
    disconnect(tcpSocket, SIGNAL(readyRead()),this,SLOT(readyToRead()));
    emit deleteRequest(socketDescriptor);
}

void THClientSocket::forceStopSocket()
{
    disconnect(TimeoutTimer,SIGNAL(timeout()),this, SLOT(stopSocket()));
    disconnect(tcpSocket, SIGNAL(disconnected()),this, SLOT(stopSocket()));
    emit messageSignal(tr("TH client disconnected(%1): %2").arg(socketDescriptor).arg(tcpSocket->peerAddress().toString()));

    tcpSocket->disconnectFromHost();
    tcpSocket->waitForDisconnected(10000);
    disconnect(tcpSocket, SIGNAL(readyRead()),this,SLOT(readyToRead()));
    emit deleteRequest(socketDescriptor);
}

int  THClientSocket::returnSocketDescriptor(){
    return socketDescriptor;
}

void THClientSocket::readyToRead(){
    QByteArray data=tcpSocket->readAll();

    QString RecievedString=QString(data);

    TimeoutTimer->start(20000); // Restartaj timer dok se primaju podaci
    // Poruka mjerenja
    if (RecievedString.startsWith("[01",Qt::CaseInsensitive) && RecievedString.endsWith("]\r\n", Qt::CaseInsensitive)){
        if(!_identified){
            ClientAddress=RecievedString.mid(3,2);
            _identified=true;
            FormulateQueryStrings();
            QDateTime TimeNow= QDateTime::currentDateTime();
            QString QueryString=QString("SELECT * FROM THParametri WHERE [Address]='%1'").arg(ClientAddress);
            m_Database->queryModel->setQuery(QueryString);
            if (m_Database->queryModel->rowCount()==0){
                // Ne postoji u bazi
                QueryString=QString("INSERT INTO THParametri (Address) VALUES ('%1')").arg(ClientAddress);
                messageSignal("Node added to database");
                m_Database->queryModel->setQuery(QueryString);

            }


            QueryString=QString("UPDATE THParametri SET [LastResponse]=#%1# WHERE [Address]='%2'").arg(TimeNow.toString("MM/dd/yyyy hh:mm:ss")).arg(ClientAddress);
            m_Database->queryModel->setQuery(QueryString);
            QString errorString=m_Database->queryModel->lastError().text();
            if (errorString!="")  errorSignal(errorString);
        }

        if (RecievedString.length()<20) return;
        MeasurementReturn Measurements= ParseMeasurementString(RecievedString);
        if (Measurements.Valid){
            QString QueryString=QString("INSERT INTO THMjerenja (Address, [GenerationTime], VIN1, VIN2, IIN1, IIN2, Batt, Temp) VALUES ('%1',%2,%3,%4,%5,%6,%7,%8)").arg(ClientAddress).arg(Measurements.Timestamp.toString("#MM/dd/yyyy hh:mm:00#")).arg(Measurements.V1).arg(Measurements.V2).arg(Measurements.I1).arg(Measurements.I2).arg(Measurements.Batt).arg(Measurements.Temp);
            m_Database->queryModel->setQuery(QueryString);
            QString errorString=m_Database->queryModel->lastError().text();
            if (errorString!="")  errorSignal(errorString);
            if (QueryStrings.size()>0){
                if (QueryStrings.at(0)!="")sendData(QueryStrings.at(0).toLatin1());  // Posalji stvari;
                QueryStrings.removeAt(0);
            }
        }else{
            errorSignal("Measurement checksum not valid");
        }
    }

    // Poruka konfiguracije
    if (RecievedString.startsWith("[02",Qt::CaseInsensitive) && RecievedString.endsWith("]\r\n\r\n", Qt::CaseInsensitive)){
        if(!_identified){
            ClientAddress=RecievedString.mid(3,2);
            _identified=true;
            FormulateQueryStrings();
        }
        QStringList ConfigStringList=RecievedString.remove("\n").split(QRegExp(","));
        QString Command;
        QString Value;
        double  ValueDouble;
        int     ValueInt;
        bool    ValueString=false;
        foreach (QString Response, ConfigStringList){
            QStringList ConfigResponseSplit=Response.split(QRegExp(" "));
            if (ConfigResponseSplit.size()!=3) continue;
            else{
                m_Database->queryModel->setQuery(QString("SELECT SetGet_Query FROM THParametri WHERE Address='%2';").arg(ClientAddress));
                QString GetSetRecord=m_Database->queryModel->record(0).value(0).toString();
                GetSetRecord.remove(ConfigResponseSplit.at(0)+" "+ConfigResponseSplit.at(1)+",");
                if (ConfigResponseSplit.at(1)=="GET"){
                    messageSignal("TH ("+ClientAddress+") reported parameter: "+ Response);
                }else if(ConfigResponseSplit.at(1)=="SET"){
                    messageSignal("TH ("+ClientAddress+") changed parameter: "+ Response);
                }else continue;
                ValueString=false;
                Command=ConfigResponseSplit.at(0);
                Value=ConfigResponseSplit[2].remove("]\r\r",Qt::CaseInsensitive);
                bool ok;

                if (m_CommandNames.contains(Command)){
                    int index=m_CommandNames.indexOf(Command);
                    if (m_CommandTypes.at(index).startsWith("STRING")) {
                        ValueString=true;
                    }
                    if (m_CommandTypes.at(index).startsWith("ENUM")) {
                        ValueInt=Value.toUInt(&ok, 16);
                        Value=QString::number(ValueInt);
                    }
                    if (m_CommandTypes.at(index).startsWith("TF")) {
                        ValueInt=Value.toUInt(&ok, 16);
                        Value=QString::number(ValueInt);
                    }
                    if (m_CommandTypes.at(index).startsWith("DT")) {

                    }
                    if (m_CommandTypes.at(index).startsWith("UINT")) {
                        ValueInt=Value.toUInt(&ok, 16);
                        Value=QString::number(ValueInt);
                    }
                    if (m_CommandTypes.at(index).startsWith("INT")) {
                        ValueInt=Value.toUInt(&ok, 16);
                        Value=QString::number(ValueInt);
                    }
                    if (m_CommandTypes.at(index).startsWith("DOUBLE")) {
                        ValueDouble=Value.toUInt(&ok, 16)/10000.0;
                        Value=QString::number(ValueDouble);
                    }
                }

                QString QueryS;
                if (ValueString==false)QueryS=QString("UPDATE THParametri SET [Rem_%1]=%2, [SetGet_Query]='%3' WHERE [Address]='%4'").arg(Command).arg(Value).arg(GetSetRecord).arg(ClientAddress);
                else QueryS=QString("UPDATE THParametri SET [Rem_%1]='%2', [SetGet_Query]='%3' WHERE [Address]='%4'").arg(Command).arg(Value).arg(GetSetRecord).arg(ClientAddress);
                qDebug()<<QueryS;
                m_Database->queryModel->clear();
                m_Database->queryModel->setQuery(QueryS);
                errorSignal(m_Database->queryModel->lastError().text());

                emit nodeGetSetResponded(ClientAddress);
            }

        }

    }

    // Poruka alarma
    if (RecievedString.startsWith("[03",Qt::CaseInsensitive) && RecievedString.endsWith("]\r\n\r\n", Qt::CaseInsensitive)){
        if(!_identified){
            ClientAddress=RecievedString.mid(3,2);
            _identified=true;
            FormulateQueryStrings();
        }
        QStringList Splitted=RecievedString.split(":");
        QDateTime TimeStamp=GetPacketTime(Splitted.at(1));
        QString AlarmString=Splitted.at(2);
        AlarmString.remove(",]\r\n\r\n");

        QString QueryString=QString("INSERT INTO THAlarmi (Address, [GenerationTime], [ActiveAlarms]) VALUES ('%1',%2,'%3')").arg(ClientAddress).arg(TimeStamp.toString("#MM/dd/yyyy hh:mm:00#")).arg(AlarmString);
        m_Database->queryModel->setQuery(QueryString);
        errorSignal(m_Database->queryModel->lastError().text());

        errorSignal("ALARM ON TH ("+ClientAddress+"): "+AlarmString);

    }
    emit dataRecieved(socketDescriptor,data);
    emit messageSignal(tr("Data recieved from(%1): ").arg(ClientAddress)+QString(data));
}
void THClientSocket::FormulateQueryStrings(){

    m_Database->queryModel->setQuery(QString("SELECT * FROM THParametri WHERE Address='%1';").arg(ClientAddress));
    if (m_Database->queryModel->rowCount()==0){
      errorSignal("Unknown node connected ("+ClientAddress+")");
      return;
    }
    QSqlRecord QueryRecord=m_Database->queryModel->record(0);

    QStringList GetSetParameters= QueryRecord.value("SetGet_Query").toString().split(",");

    QDateTime T= QDateTime::currentDateTime();
    QueryStrings.append("[CONFIG TIME SET "+T.toString("hh:mm:ss:dd:MM:yyyy")+"]");

    for (int i=0; i<GetSetParameters.size();i++){

        QString Entry=GetSetParameters.at(i);
        if (Entry.contains("GET")){
            QueryStrings.append("[CONFIG "+Entry+" 0]");
        }else if (Entry.contains("SET")){
            Entry.remove(" SET",Qt::CaseInsensitive);
            QString Command=Entry;
            m_Database->queryModel->setQuery(QString("SELECT Rem_%1 FROM THParametri WHERE Address='%2';").arg(Command).arg(ClientAddress));
            if (m_Database->queryModel->rowCount()==0){
              errorSignal("Problem reading reading query setting from databse");
              return;
            }
            QSqlRecord QueryValueRecord=m_Database->queryModel->record(0);
            double          ValueDouble;
            unsigned int    ValueInt;

            bool ok;

            if (m_CommandNames.contains(Command)){
                int index=m_CommandNames.indexOf(Command);
                if (m_CommandTypes.at(index).startsWith("STRING")) {
                     QueryStrings.append("[CONFIG "+GetSetParameters.at(i)+" "+QueryValueRecord.value(0).toString()+"]");
                }
                if (m_CommandTypes.at(index).startsWith("ENUM")) {
                    ValueInt=QueryValueRecord.value(0).toUInt();
                    QueryStrings.append("[CONFIG "+GetSetParameters.at(i)+" "+QString::number(ValueInt,16)+"]");
                }
                if (m_CommandTypes.at(index).startsWith("TF")) {
                    ValueInt=QueryValueRecord.value(0).toUInt();
                    QueryStrings.append("[CONFIG "+GetSetParameters.at(i)+" "+QString::number(ValueInt,16)+"]");
                }
                if (m_CommandTypes.at(index).startsWith("DT")) {

                }
                if (m_CommandTypes.at(index).startsWith("UINT")) {
                    ValueInt=QueryValueRecord.value(0).toUInt();
                    QueryStrings.append("[CONFIG "+GetSetParameters.at(i)+" "+QString::number(ValueInt,16)+"]");
                }
                if (m_CommandTypes.at(index).startsWith("INT")) {
                    ValueInt=QueryValueRecord.value(0).toUInt();
                    QueryStrings.append("[CONFIG "+GetSetParameters.at(i)+" "+QString::number(ValueInt,16)+"]");
                }
                if (m_CommandTypes.at(index).startsWith("DOUBLE")) {
                    ValueDouble=QueryValueRecord.value(0).toDouble(&ok);
                    ValueInt=ValueDouble*10000;
                    QueryStrings.append("[CONFIG "+GetSetParameters.at(i)+" "+QString::number(ValueInt,16)+"]");
                }
            }
        }
    }
}

THClientSocket::MeasurementReturn THClientSocket::ParseMeasurementString(QString String){
    MeasurementReturn Return;

    bool ok;
    QRegExp rx("(\\[|\\]|\\:)");
    QStringList Parts = String.split(rx);


    QDateTime TimeStamp=GetPacketTime(Parts.at(2));
    Return.Address=Parts.at(1).mid(0,2).toInt(&ok,16);
    Return.Timestamp=TimeStamp;
    Return.V1=Parts.at(3).mid(0,4).toUInt(&ok,16)*1.0/10000.0;
    Return.V2=Parts.at(3).mid(4,4).toUInt(&ok,16)*1.0/10000.0;
    Return.I1=Parts.at(3).mid(8,4).toUInt(&ok,16)*1.0/10000.0;
    Return.I2=Parts.at(3).mid(12,4).toUInt(&ok,16)*1.0/10000.0;
    Return.Temp=Parts.at(3).mid(16,4).toInt(&ok,16)*1.0/1000.0;
    if(Return.Temp>50) Return.Temp-=65.535;

    Return.Batt=Parts.at(3).mid(20,4).toInt(&ok,16)*1.0/10000.0;
    // TODO: Fali provjera checksuma
    Return.Valid=true;

    return Return;
}

QDateTime THClientSocket::GetPacketTime(QString TimeString){

    QDate Date;
    QTime Time;
    QDateTime PacketDateTime(QDateTime::currentDateTime());
    int PacketHour  =TimeString.mid(0,2).toInt();
    int PacketMin   =TimeString.mid(2,2).toInt();
    int PacketDay   =TimeString.mid(4,2).toInt();
    int PacketMonth =TimeString.mid(6,2).toInt();
    int PacketYear  =TimeString.mid(8,2).toInt();

    Date.setDate(PacketYear+2010,PacketMonth,PacketDay);
    Time.setHMS(PacketHour,PacketMin,0);
    PacketDateTime.setDate(Date);
    PacketDateTime.setTime(Time);
    PacketDateTime.setTimeSpec(Qt::LocalTime);

    return PacketDateTime;
}



THTCPServer::THTCPServer(QObject *parent, mySQL *database, QStringList CommandStrings) : QTcpServer(parent)
{
    activeTHClientSocketList=new QList<THClientSocket *>();
    activeTHClientSocketList->clear();
    m_Database=database;
    parseCommadStrings(CommandStrings);

}

THTCPServer::~THTCPServer(){

    emit errorSignal("Stopping server");
    // dilitati ovdje sta treba
}

void THTCPServer::incomingConnection(int socketDescriptor)
{
    THClientSocket *thread = new THClientSocket(socketDescriptor, this, m_Database,m_CommandNames,m_CommandTypes);
    activeTHClientSocketList->append(thread);
    connect(thread, SIGNAL(deleteRequest(int)),this, SLOT(requestToDeleteHandler(int)));
    connect(thread, SIGNAL(messageSignal(QString)), this, SLOT(messageSignalRecieved(QString)));
    connect(thread, SIGNAL(errorSignal(QString)), this, SLOT(errorSignalRecieved(QString)));
    connect(thread, SIGNAL(nodeGetSetResponded(QString)), this, SIGNAL(nodeGetSetResponded(QString)));
    emit messageSignal("New client connecting...");
    thread->startSocket();
}

QList<QHostAddress> THTCPServer::returnIPs(){

    return QNetworkInterface::allAddresses();

}

void THTCPServer::startServer(int port)
{


    if (!this->listen(QHostAddress::Any,port)) {
        emit errorSignal(tr("Unable to open server: ").arg(this->errorString()));
        close();
        return;
    }
    emit messageSignal("Starting server...");
    QString ipAddress;
    QList<QHostAddress> ipAddressList=returnIPs();
    emit messageSignal(tr("Server running on port %1").arg(port));
    emit messageSignal("Server listening on adresses:");


    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressList.size(); i++) {
        if (ipAddressList.at(i).protocol()== QAbstractSocket::IPv4Protocol){
            ipAddress = ipAddressList.at(i).toString();
            emit messageSignal(ipAddress+"\n");
        }
    }

    _IPAddressChoice=ipAddress;
    _portChoice=port;
}

void THTCPServer::requestToDeleteHandler(int _socketDescriptor){
    for (int i=0;i<activeTHClientSocketList->size();i++){
        if (_socketDescriptor==activeTHClientSocketList->at(i)->returnSocketDescriptor()){
            qDebug()<<"removing from list 1"<<_socketDescriptor;
            activeTHClientSocketList->at(i)->deleteLater();
            activeTHClientSocketList->removeAt(i);
            qDebug()<<"removing from list 2"<<_socketDescriptor;
        }
    }
}

void THTCPServer::stopServer(){

    emit messageSignal("Stopping server...");
    this->close();
    emit messageSignal(this->errorString());
    emit messageSignal("Server stopped");
}

void THTCPServer::errorSignalRecieved(QString ErrorInfo){
    emit errorSignal(ErrorInfo);
}

void THTCPServer::messageSignalRecieved(QString MessageInfo){
    emit messageSignal (MessageInfo);
}


void THTCPServer::parseCommadStrings(QStringList CommandStrings)
{
    foreach(QString s,CommandStrings){
        if (s=="") continue;
        else{
            QStringList parameters=s.split(" ");
            m_CommandNames.append(parameters.at(0));
            m_CommandTypes.append(parameters.at(1));
        }
    }
}
