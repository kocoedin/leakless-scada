#ifndef THWIDGET_H
#define THWIDGET_H

#include <QTabWidget>
#include "plottingwidget.h"
#include "thwidget.h"
#include "thtcpserver.h"
#include "mapwidget.h"
#include "getsetcombobox.h"

namespace Ui {
class THWidget;
}

class THWidget : public QWidget
{
    Q_OBJECT

public:
    explicit THWidget(QMainWindow *parent = 0, mySQL *Database=0, MapWidget *Map=0);
    ~THWidget();

    mySQL       *database;              // Pokazivac na bazu podatak
    void updateNodeInfo(QString NodeAddress);
    void updateGetSet(QString NodeAddress);
    void updateMeasurements(QString NodeAddress);
    void updateAlarmList();
    void updateEmailList();
    void fillComboBox();
    void refreshActiveConnections();
    void FindAllTreeItems(QTreeWidgetItem *item);
    void StartTHServer();

public slots:

    void THConfigResponded(QString Address);
    void closeEvent(QCloseEvent *event);
    void showWidget(int TabNumber);
    void showNodesOnMap();

private slots:

    void THcontextMenu(QPoint point);
    void GetSetIndexChanged(QString CommandName, int CurrentIndex);

    void on_tableWidget_cellClicked(int row, int column);
    void nodeSelected(int ID);
    void on_dialDays_valueChanged(int value);
    void on_dialHours_valueChanged(int value);
    void on_comboBox_currentIndexChanged(const QString &arg1);

    void plotSelectedMeasurements();


    /* Ovo je za mjerenja */
    void VIn1_Description_changed(QString Value);
    void VIn2_Description_changed(QString Value);
    void IIn1_Description_changed(QString Value);
    void IIn2_Description_changed(QString Value);
    void Temp_Description_changed(QString Value);
    void Batt_Description_changed(QString Value);

    void VIn1_Unit_changed(QString Value);
    void VIn2_Unit_changed(QString Value);
    void IIn1_Unit_changed(QString Value);
    void IIn2_Unit_changed(QString Value);
    void Temp_Unit_changed(QString Value);
    void Batt_Unit_changed(QString Value);

    void VIn1_Gain_changed(double Value);
    void VIn2_Gain_changed(double Value);
    void IIn1_Gain_changed(double Value);
    void IIn2_Gain_changed(double Value);
    void Temp_Gain_changed(double Value);
    void Batt_Gain_changed(double Value);

    void on_dial_Days_Alarm_valueChanged(int value);
    void on_dial_Hours_Alarm_valueChanged(int value);

    void on_alarmListWidget_customContextMenuRequested(const QPoint &pos);
    void on_connectionsListView_customContextMenuRequested(const QPoint &pos);
    void on_tableWidget_itemChanged(QTableWidgetItem *item);

    void on_mailCheckBox_toggled(bool checked);
    void on_mailList_itemSelectionChanged();
    void on_addMailButton_clicked();
    void on_removeMailButton_clicked();


    void on_SaveConfigurationButton_clicked();
    void on_serwerPowerUp_clicked();
    void on_serverPowerDown_clicked();



    void ComboBox_changed(int Value);
    void TextBox_changed(QString Value);
    void CheckBox_changed(bool Value);
    void DateTimeBox_changed(QDateTime Value);
    void SpinBox_changed(int Value);
    void DoubleSpinBox_changed(double Value);

    void errorSignalHandler(QString ErrorInfo);
    void messageSignalHandler(QString MessageInfo);

signals:

    void serverPowerDownRequest();
    void serverPowerUpRequest();
    void plotRequest(PlottingWidget *chosenPlottingWidget);
    void errorSignal(QString ErrorInfo);
    void messageSignal(QString MessageInfo);

private:
    void refreshTHList();


    /* Ovo je za odabir mjerenja */
    QLineEdit VIn1_Description;
    QLineEdit VIn1_Unit;
    QDoubleSpinBox VIn1_Scale;
    QLineEdit VIn2_Description;
    QLineEdit VIn2_Unit;
    QDoubleSpinBox VIn2_Scale;
    QLineEdit IIn1_Description;
    QLineEdit IIn1_Unit;
    QDoubleSpinBox IIn1_Scale;
    QLineEdit IIn2_Description;
    QLineEdit IIn2_Unit;
    QDoubleSpinBox IIn2_Scale;
    QLineEdit Temp_Description;
    QLineEdit Temp_Unit;
    QDoubleSpinBox Temp_Scale;
    QLineEdit Batt_Description;
    QLineEdit Batt_Unit;
    QDoubleSpinBox Batt_Scale;

    QList<getSetComboBox*> ListOfGetSetComboBoxes;


    // Server configuration postavke

    QLineEdit Manufacturer;
    QLineEdit User;
    QLineEdit ResponsiblePerson;
    QLineEdit InstallationDate;
    QLineEdit DBLocation;
    QLineEdit DBConnectionString;
    QLineEdit ServerPort;

    QLineEdit MailPort;
    QLineEdit MailPassword;
    QLineEdit MailService;
    QLineEdit Mail;

    QCheckBox ReportIncomingConnections;
    QCheckBox ReportIncomingData;
    QCheckBox ReportOutgoingData;
    QCheckBox ReportAlarms;
    QCheckBox ReportParameterConfig;

    QString CurrentSelectedTHAddress;

    Ui::THWidget *ui;
    QString m_SensorType;
    MapWidget *m_Map;

    QStringList CommandStrings;

    THTCPServer *TcpServer;

    QList<QString> Commands;
    QList<QTreeWidgetItem *> Items;

    bool m_pressedItemState;
};




#endif





