#include "usercontrolandlogging.h"


UserControlAndLogging::UserControlAndLogging(QObject *parent) :  QObject(parent)
{
    CurrentUser.logged_in=false;
    userLoginWindow = new LoginDialog();


}

UserControlAndLogging::UserControlAndLogging(QObject *parent, mySQL *Database) : QObject(parent)
{
    CurrentUser.logged_in=false;
    userLoginWindow = new LoginDialog();
    database=Database;
}

void UserControlAndLogging::RequestUserLogin(){
    CurrentUser.logged_in=false;
    connect(userLoginWindow, SIGNAL(cancelButtonPressed()),this, SLOT(cancelButtonPressed()));
    connect(userLoginWindow, SIGNAL(connectButtonPressed(QString,QString,int)),this, SLOT(connectButtonPressed(QString,QString,int)));
    userLoginWindow->clearEntries();
    userLoginWindow->changeStatusBarText("Enter username and password");
    userLoginWindow->setModal(true);
    userLoginWindow->exec();   //  Prikazi prozor

}

void UserControlAndLogging::RequestUserLogout(){

    if (CurrentUser.logged_in==true){
        CurrentUser.logged_in=false;
        QDateTime dateTime = QDateTime::currentDateTime();
        QString dateTimeString = dateTime.toString("yyyy-MM-dd hh:mm:ss");
        database->queryModel->setQuery(QString("INSERT INTO userloginlog([Timestamp], [UserAccounts_idUserAccounts, Login, Logout, Info) VALUES ('%1', (SELECT idUserAccounts FROM useraccounts WHERE Username='%2') ,'0', '1', 'User: %3 logged out');").arg(dateTimeString,CurrentUser.Username, CurrentUser.Username));

    }



    emit logoutSignal();

}

UserControlAndLogging::UserInfo UserControlAndLogging::ReturnCurrentUser(){

    UserInfo Return;

    return Return;

}

void UserControlAndLogging::WriteActionToLog(QString ActionInfo){


}

void UserControlAndLogging::connectButtonPressed(QString Username, QString Password, int type){

    if(type==0){
        database->queryModel->clear();
        database->queryModel->setQuery(QString("SELECT *  FROM UserAccounts WHERE UserName LIKE '%1' AND Password LIKE '%2'; ").arg(Username,Password));

        if(database->queryModel->rowCount()==1){

            userLoginWindow->changeStatusBarText("Connected");

            CurrentUser.logged_in=true;
            CurrentUser.Username=Username;
            CurrentUser.sensor_permission_1     = database->queryModel->record(0).value("SensorPermission1").toBool();
            CurrentUser.sensor_permission_2     = database->queryModel->record(0).value("SensorPermission2").toBool();
            CurrentUser.control_permission_1    = database->queryModel->record(0).value("ControlPermission1").toBool();
            CurrentUser.control_permission_2    = database->queryModel->record(0).value("ControlPermission2").toBool();
            CurrentUser.aditional_permission_1  = database->queryModel->record(0).value("AdminPermission1").toBool();
            CurrentUser.aditional_permission_2  = database->queryModel->record(0).value("AdminPermission2").toBool();
            CurrentUser.type=0;
            QDateTime dateTime = QDateTime::currentDateTime();
            QString dateTimeString = dateTime.toString("yyyy-MM-dd hh:mm:ss"); //SELECT ID FROM UserAccounts WHERE UserName='%2'
            QString queryString= QString("INSERT INTO UserLoginLog ([Timestamp], [Login], [Logout], [Info]) VALUES ('%1','1', '0', 'User: %2 logged in');").arg(dateTimeString).arg(Username);

            database->queryModel->setQuery(queryString);

            QString error= database->queryModel->lastError().text();
            userLoginWindow->close();
            emit loginSignal();

        }else if(database->queryModel->rowCount()>1){
            userLoginWindow->changeStatusBarText("Multiple users detected");
        }else{
            userLoginWindow->changeStatusBarText("Error");
        }
    }else if (type==1){
        if (Username=="kocoedin" && Password=="38023802"){
            CurrentUser.logged_in=true;
            CurrentUser.Username=Username;
            CurrentUser.sensor_permission_1     = 1;
            CurrentUser.sensor_permission_2     = 1;
            CurrentUser.control_permission_1    = 1;
            CurrentUser.control_permission_2    = 1;
            CurrentUser.aditional_permission_1  = 1;
            CurrentUser.aditional_permission_2  = 1;
            CurrentUser.type=1;

            userLoginWindow->close();
            emit loginSignal();
        }
        else userLoginWindow->changeStatusBarText("Error");
    }
}

void UserControlAndLogging::cancelButtonPressed(){

    userLoginWindow->changeStatusBarText("Error");
    emit logoutSignal();

    //QApplication::quit();
}
