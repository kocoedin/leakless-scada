#ifndef USERCONTROLANDLOGGING_H
#define USERCONTROLANDLOGGING_H

#include <QObject>
#include "logindialog.h"
#include "mysql.h"

class UserControlAndLogging : public QObject
{
    Q_OBJECT

    typedef struct {
        bool logged_in;
        QString Username;
        bool sensor_permission_1;
        bool sensor_permission_2;
        bool control_permission_1;
        bool control_permission_2;
        bool aditional_permission_1;
        bool aditional_permission_2;
        int type;
    }UserInfo;

public:




    UserInfo CurrentUser;

    // Metode
    explicit UserControlAndLogging(QObject *parent = 0);
    UserControlAndLogging(QObject *parent, mySQL *Database);

    void RequestUserLogin();
    void RequestUserLogout();

    UserInfo ReturnCurrentUser();

    void WriteActionToLog(QString ActionInfo);

    // Varijable
    mySQL       *database;              // Pokazivac na bazu podataka
    LoginDialog *userLoginWindow;       // Prozor za prijavu korisnika

private:
    void WriteLoginToLog(QString Username, QString Info);
    void WriteLogoutToLog(QString Username, QString Info);

public slots:
    void connectButtonPressed(QString Username, QString Password, int type);
    void cancelButtonPressed();

signals:
    void logoutSignal();
    void loginSignal();

public slots:

};

#endif // USERCONTROLANDLOGGING_H
